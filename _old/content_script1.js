$(document).ready(function() {
	// 스크립트 실행 메시지 수신을 위한 메시지 리스너 설정
	chrome.runtime.onMessage.addListener(
			function(request, sender, sendResponse) {
				if (request.action) {
					if (request.action === 'excuteScript') {
						nsHelperInit();
					}
					sendResponse({result: "SUCCESS"});
				}
				else {
					sendResponse({result: "FAIL"});
				}
			});
});

// 페이지 초기화
// 메시지 전송 포트 생성. 이벤트핸들러 설정
function nsHelperInit() {
	$("*").click(function(e) {
		e.preventDefault();
		var selector = nsHelperGetSelector($(this));
		console.log(selector + " clicked");
		chrome.runtime.sendMessage({action: "select", selector: selector, text: $(selector).text().trim().substring(0,100)}, function(response) {
			console.log(response.result);
		});
		return false;
	});

	$("*").mouseenter(function() {
		$(this).addClass("nsHelper-border");
		$(this).parents().removeClass("nsHelper-border");
	});

	$("*").mouseleave(function() {
		if ($(this).tagName === "body") {
			return;
		}
		$(this).removeClass("nsHelper-border");
	});
}

// 선택된 DOM객체의 selector 조회
function nsHelperGetSelector(el)
{
	var $el = jQuery(el);

	var selector = $el.parents(":not(html,body)")
	.map(function() { 
		var i = jQuery(this).index(); 
		i_str = ''; 

		if (typeof i != 'undefined') 
		{
			i = i + 1;
			i_str += ":nth-child(" + i + ")";
		}

		return this.tagName + i_str; 
	})
	.get().reverse().join(" ");

	if (selector) {
		selector += " "+ $el[0].nodeName;
	}

	var index = $el.index();
	if (typeof index != 'undefined')  {
		index = index + 1;
		selector += ":nth-child(" + index + ")";
	}

	return selector;
}
