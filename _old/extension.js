// Copyright (c) 2011 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

chrome.runtime.onMessage.addListener(onMessageHandler);

function onMessageHandler(request, sender, sendResponse) {
	console.log(request.action + " sent");
	if (request.action) {
		if (request.action === 'select') {
			document.form1.selector_sent.value = request.selector;
			document.form1.text_sent.value = request.text;
		}
		sendResponse({result:"SUCCESS"});
	}
	else {
		sendResponse({result:"FAIL"});
	}
}

$(document).ready(function() {
	$(".btnStart").click(function(e) {
		console.log('btnStart clicked');
		e.preventDefault();
		chrome.tabs.query({active: true, currentWindow: false}, function(tabs) {
			chrome.tabs.sendMessage(tabs[0].id, {action: "excuteScript"}, function(response) {
				console.log(response);
			});
		});
	});
	
	$(".btnSelect").click(function(e) {
		console.log('btnSelect clicked');
		e.preventDefault();
		var selector = $(this).closest('tr').find('#selector');
		var sampleData = $(this).closest('tr').find('#sampleData');
		selector.val($('#selector_sent').val());
		sampleData.val($('#text_sent').val());
	});
	
	$(".btnDelete").click(function(e) {
		console.log('btnDelete clicked');
		e.preventDefault();
		var selector = $(this).closest('tr').find('#selector');
		var sampleData = $(this).closest('tr').find('#sampleData');
		selector.val('');
		sampleData.val('');
	});

	$(".btnSave").click(function(e) {
		console.log('btnSave clicked');
		e.preventDefault();
		alert('작업중입니다.');
	});

	$(".btnCancel").click(function(e) {
		console.log('btnCancel clicked');
		e.preventDefault();
		if (confirm('취소 하시겠습니까?')) {
			document.form1.reset();
		}
	});

});
