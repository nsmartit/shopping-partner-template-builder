// Copyright (c) 2011 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

var nsHelperWin = null; 

// Called when the user clicks on the browser action.
chrome.browserAction.onClicked.addListener(function(tab) {
	// 셀렉터 등록 팝업 띄움
	//var p1 = 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no'; 
	var p1 = 'status=no,location=no,toolbar=no,menubar=no,' 
		+ 'width=1000,height=600,left=100,top=100';
	nsHelperWin = window.open("nsSelectorInsertPopup.html", "popup", p1);
});
