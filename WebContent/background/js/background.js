(function() {
	/**
	 * Background 처리를 위한 객체를 정의 함.
	 */

	$background = {
		init : function() {
			$background.extenApp.bindActionEvent();
		},
		extenApp : {
			/**
			 * 확장 프로그램
			 */

			bindActionEvent : function() {
				/**
				 * Browser의 이벤트를 설정 한다.
				 */
				// Called when the user clicks on the browser action.
				chrome.browserAction.onClicked.addListener(function(tab) {
					$background.appBrowser.open();
				});
				
				// Window로 부터 이벤트를 수신 했을 때...
//				window.addEventListener("message", function(e) {
//					alert(e.data);
//				});
				chrome.runtime.onConnect.addListener(function(port) {
					const $msg = $background.appBrowser.msg;
					switch(port.name) {
					case "ns-template-builder-browser":
						$msg.browser.port = port;
						port.onMessage.addListener(function(msg) {
							$msg.browser.receive(msg);
						});
						break;
					case "ns-template-builder-contents":
						$msg.contents.port = port;
						port.onMessage.addListener(function(msg) {
							$msg.contents.receive(msg);
						});
						const $appBrowser = $background.appBrowser;
						// 새로고침 한 경우, 팝업창이 이미 열려 있다면, contextMenu를 On 시킴
						if(!$appBrowser.isClosed()) {
							$msg.contents.send({action:"on"});
						}
						break;
					}
				});
			},
			name : "extenApp"
		},
		appBrowser : {

			_window : undefined,
			open : function() {
				/**
				 * Template Builder 팝업 띄움
				 */
				
				let openWindow = function(left, top, width, height, page) {
					/**
					 * 팝업창을 띄움
					 */
					let params = new Array();
					params.push(`status=no`);
					params.push(`location=no`);
					params.push(`toolbar=no`);
					params.push(`menubar=no`);
					params.push(`width=${width}`);
					params.push(`height=${height}`);
					params.push(`left=${left}`);
					params.push(`top=${top}`);
					params.push(`fullscreen=3`);

					let win = window.open(page, "popup", params.join(","));
					$background.appBrowser._window = win;
					
					return win;
				};
				let resizeWindow = function(left, top, width, height, wndId) {
					/**
					 * Contents 브라우져의 크기를 조절 함.
					 */
					let updateInfo = {
						    left: left,
						    top: top,
						    width: width,
						    height: height
						};
						chrome.windows.update(wndId, updateInfo);
				};
				
				let refreshContWin = () => {
					// 현재 페이지를 새로고침 함. //
					chrome.tabs.query({
						active : true,
						currentWindow : true
					}, function(tabs) {
						try{
							chrome.tabs.reload(tabs[0].id);
						}
						catch(e) {
							$message.alert.show(e);
						}
					});
				};
				
				if (this.isClosed()) {
					// 0000000000000000000000000000000000000000000000000000000000
					chrome.windows.getCurrent(function(wind) {
						let maxWidth = window.screen.availWidth;
						let maxHeight = window.screen.availHeight;
						
						// Template Builder의  Width 계산
						let selectorWinW = Math.ceil(maxWidth * 0.3);
						// Template Builder 창을 띄움
						let win = openWindow(0, 0, selectorWinW, maxHeight, "/foreground/view/layout.html");
						// Contents Browser 창의 위치, 크기 재조정
						resizeWindow(selectorWinW, 0, maxWidth - selectorWinW, maxHeight, wind.id);

						const $msg = $background.appBrowser.msg;

						// waitting for windows close //
						$(win).bind("beforeunload", function (e){
							const $appBrowser = $background.appBrowser;
							if(!$appBrowser.isClosed()) {
								$msg.contents.send({action:"off"});
							    $appBrowser.close();
							}
						});

					});
					
					refreshContWin();
					// 0000000000000000000000000000000000000000000000000000000000
				} else {
					// 해당 윈도우를 화면에 띄움.
//					this._window.focus();
					this.close();
				}

				return this._window;
			},
			close : function() {
				/**
				 * Template Builder 팝업 닫음
				 */
				const $msg = $background.appBrowser.msg;
				if (!this.isClosed()) {
					if(this._window != undefined) {
						this._window.close();
					}
					this._window = undefined;
				}
			},
			isClosed : function() {
				/**
				 * Template Builder 팝업이 닫혔는지 여부를 반환 함.
				 */
				return (this._window == undefined) || (this._window.closed);
			},
			msg : {
				browser : {
					port : undefined,
					postMessage : function(msg) {
						const $appBrowser = $background.appBrowser;
						if(!$appBrowser.isClosed()) {
							$appBrowser._window.postMessage(msg, window.origin);
						}
					},
					send : function(msg) {
						const $port = this.port;
						if($port != undefined) {
							$port.postMessage(msg);
						}
					},
					receive : function(data) {
					},
					enableSelector : function() {
						this.postMessage("enableSelector");
					},
					desableSelector : function() {
						this.postMessage("desableSelector");
					}
					
				},
				contents : {
					port : undefined,
					send : function(msg) {
						const $port = this.port;
						if($port != undefined) {
							$port.postMessage(msg);
						}
					},
					receive : function(data) {
					}
				},
				name : "msg"
			},
			name : "appBrowser"
		},
		name : "$background"
	};

	// this is background script.. not use document.ready event!!!!
	$(document).ready(function() {
		$background.init();
	});

})();
