(function() {

	$contextMenus = {


		init : function() {
			
			const menuA = [
					  { id: '열기', act: (info, tab) => { $contextMenus.event.openWindow(); } },
					  { id: '닫기', act: (info, tab) => { $contextMenus.event.closeWindow(); } },
					  { id: '시작', act: (info, tab) => { $contextMenus.event.startPickUI(); } },
					  { id: '중지', act: (info, tab) => { $contextMenus.event.stopPickUI(); } }
					];
			// sub menu example
//			const rootMenu = [
//					  //
//					  // In real practice you must read
//						// chrome.contextMenus.ACTION_MENU_TOP_LEVEL_LIMIT
//					  //
//					  { id: '캡쳐', act: (info, tab) => { console.log('Clicked ItemA', info, tab, info.menuItemId); alert('Clicked ItemA') }, menu: menuA }
//					];

				
			$contextMenus.registContextMenus(menuA);
		},
		registContextMenus : function(rootMenu) {
			let listeners = {};
			let contexts = ['browser_action'];
			let addMenu = (menu, root = null) => {
				for (let item of menu) {
					let {id, menu, act} = item;

					chrome.contextMenus.create({
						id: id,
						title: id,
						contexts: contexts,
						parentId: root
					});
			
					if (act) {
						listeners[id] = act;
					}
			
					if (menu) {
						addMenu(menu, id);
					}
				}
			};

			addMenu(rootMenu);

			chrome.contextMenus.onClicked.addListener((info, tab) => {
				console.log('Activate „chrome.contextMenus -> onClicked Listener“', info, tab);
				// -----------------------------------------------------
				chrome.tabs.query(
					{active : true, currentWindow : false},
					function(tabs) {
						if(tabs.length>1) {
							alert("브라우져가 여러개 열려 있습니다.\n다른 브라우져를 닫아 주세요.");
						} else {
							listeners[info.menuItemId] (info, tab);
						}
				});
				// -----------------------------------------------------
				
			});	
		}, 
		event : {
			openWindow : function() {
				$background.appBrowser.open();
			},
			closeWindow : function() {
				$background.appBrowser.close();
			},
			startPickUI : function() {
				// browser -> contents 영역으로 enableSelector Msg를 전달 한다.
				 $background.appBrowser.msg.browser.enableSelector();
			},
			stopPickUI : function() {
				// browser -> contents 영역으로 desableSelector Msg를 전달 한다.
				 $background.appBrowser.msg.browser.desableSelector();
			}
		}

	

	}; // eof $contextMenus

	$(document).ready(function(){
		$contextMenus.init();
	});
})();
