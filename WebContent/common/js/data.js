(function($$buff) {
	
	const DEFAULT_ID_WORK = "DEF";
	const GROUP_ID_FIELD = "NS-";
		
	const PATH_SEP_STR = " > ";
	
	const SYNC_URL = "http://nmz.kro.kr:8080";
//	const SYNC_URL = "http://127.0.0.1:8080";
	
	$data = {
		init : function() {
			// Repository를 비움...
			// $$buff.clear();
			
			// 서버에서 받아 옴..
			
		},
		storage : {
// parent : $data,
			cons : {
				key : {
					siteInfo : "SITE_INFO",
					item : "ITEM",
					action : "ACTION",
					view : "VIEW",
					layout : "LAYOUT_DATA",

					name : "$data.storage.cons.key"
				},
				name : "$data.storage.cons"
			},
			
			synch : {
				save : function(fnCallback) {

					
					let sendInfoToServer = function(groupId, fnCallback) {
						let sendData = {
							"name" : groupId,
							"contents" : JSON.stringify($data.storage.io.core.group.get(groupId))
						};
						$net.post.send(`${SYNC_URL}/template/saveTemplateWork`, sendData, fnCallback);
					}
					
					// 서버에 저장된 데이터 삭제
					let serverIds = [];
					$net.post.send(`${SYNC_URL}/template/selectTemplateWorkList`, {}, function(data){
						if(data.code == "00") {
							data.data.forEach((item, idx)=>{
								serverIds.push(item.name);
//								$net.post.send(`${SYNC_URL}/template/deleteTemplateWork`, {"name":item.name}, function(data){});
							});
						}
					});
					
//					// 현재 작업중인 사항 저장(이건 좀 더 상황을 보고....)
//					sendInfoToServer(DEFAULT_ID_WORK, "http://test/test/reg", fnCallback);
//					// 현재 화면 클릭 상태 저장(이것도 상황을 좀 더 보고...)
//					sendInfoToServer($data.storage.cons.key.layout, "http://test/test/reg", fnCallback);
					let fnDelItem = function(arr, str) {
						let idx = arr.indexOf(str);
						if(idx>0) {
							arr = arr.splice(0, idx).concat(arr.splice(1));
						}else if(idx==0) {
							arr = arr.splice(1);
						}
						return arr;
					}

					serverIds = fnDelItem(serverIds, DEFAULT_ID_WORK);
					sendInfoToServer(DEFAULT_ID_WORK);

					serverIds = fnDelItem(serverIds, $data.storage.cons.key.layout);
					sendInfoToServer($data.storage.cons.key.layout);

					$data.storage.io.group.getIds().forEach(function(groupId, idx) {
						if(groupId.indexOf("$") != 0) {
							serverIds = fnDelItem(serverIds, groupId);
							sendInfoToServer(groupId, fnCallback);
						}
					});

//					// 삭제된(존재하지 않는) group 서버에서 삭제 시켜 버림..
					// ----------------------------------------------------------------------------------------------------------------
					// 주의!! 만약 동시에 작업하는 환경이 되면, 이렇게 처리 하면 큰일 남.. 서버에서 전체 목록을 갱신하도록 하던가, 건별로 갱신하도록 변경 해야 함...!!!!
					// ----------------------------------------------------------------------------------------------------------------
					serverIds.forEach((item, idx)=>{
						$net.post.send(`${SYNC_URL}/template/deleteTemplateWork`, {"name":item}, function(data){});
					});
				},
				load : function(fnCallback) {
					$net.post.send(`${SYNC_URL}/template/selectTemplateWorkList`, {}, function(data){
						if(data.code == "00") {
							$$buff.clear();
							
							// 성공
							data.data.forEach((item, idx)=>{
								$$buff.setItem(item.name, JSON.stringify(JSON.parse(item.contents)));
							});
						}
						if(fnCallback != undefined) {
							fnCallback(data);
						}
						
						window.location.reload();
					});
				},
				build : function(groupId, fnCallback) {
					const $group = $data.storage.io.group;
					const $items = $data.storage.io.items;
					
					
					// 최종 데이터를 생성 함.
					// 필수 체크 항목을 생성 함.
					let fnMust = function() {
						let items = $data.storage.io.items.get();
						let keys = $(Object.keys(items)).filter((idx,key)=>{return typeof(items[key].must) != "undefined" && "Y" == items[key].must});
						let retItems = [];
						keys.each(function(idx, key){
							retItems.push($data.util.getFullPath(items[key]));
						});
						return retItems;
					};
					
					let fnGetView = function(viewId) {
						let views = $data.storage.io.views.get(viewId);
						let items = $data.storage.io.items.get();
						let retval = [];
						
						views.forEach((item, idx)=>{
							let jsObj = items[item.uuid];
							retval.push({
								name : jsObj.alias,
								path : $data.util.getFullPath(jsObj),
								attrType : jsObj.attrType,
								attrName : jsObj.attrName
							});
						});
						
						return retval;
					};
					let fnAbsPathByAlias = function(alias){
						const $items = $data.storage.io.items;
						const $util = $data.util;

						const items = $items.get();
						let findKey = Array.from(Object.keys(items)).find(o=>{return items[o].alias == alias});
						let findItem =  items[findKey];
						return findItem != undefined ? $util.getFullPath(findItem) : undefined;
					};
					let fnGetAction = function() {
						const $actions = $data.storage.io.actions;
						const $items = $data.storage.io.items;
						
						let items = $data.storage.io.items.get();
						let actions = $actions.get();
						
						let retval = [];
						actions.forEach((item, idx)=>{
							let jsObj ={
									action : item.action
								};
							switch(item.action) {
							case "click": {
								jsObj.path = $data.util.getFullPath(items[item.uuid]);
							}
							break;
							case "startLoop": {

								
								if(item.condType == "element") {
									// 같은 tag명으로 반복
									jsObj.loopOpt = {
											type : item.condType,
											value : fnAbsPathByAlias(item.condVal)
										};
								} else if(item.condType == "count") {
									// 횟수로 반복
									jsObj.loopOpt = {
											type : item.condType,
											value : item.condVal
										};
								}

							}
							break;
							case "read":{
								debugger;
								if(item.condVal == undefined) {
									item.condVal = $data.storage.io.views.getIds()[0];
								}
								jsObj.name = item.condVal;
								jsObj.items = fnGetView(item.condVal);
							}
							break;
							}
							retval.push(jsObj);
						});
						return retval;
					};
					
					let url = $data.storage.io.siteInfo.get("url");
					let siteInfo = {
							url : url.length>0 ? url[0] : "",				// ARRAY : 적용할 URL
							must : fnMust(),								// ARRAY : 해당 URL의 화면에서 필수로 존재하여야 하는 Element 목록
							action : fnGetAction()
						};
					
					// Action 데이터를 생성 함.
					$$buff.setItem(`$BUILD$-${groupId}`, JSON.stringify(siteInfo));
					
					// send To Server 
					let sendData = {
							name : groupId,
							domain : siteInfo.url,
							contents : JSON.stringify(siteInfo)
						};
					$net.post.send(`${SYNC_URL}/template/saveTemplateInfo`, sendData, fnCallback);

				},
				name : "$data.storage.synch"
			},
			
			io : {
				core : {
					group : {
						set : function(jsGorup, groupId) {
							const nGroupId = groupId == undefined ? DEFAULT_ID_WORK : groupId;
							$$buff.setItem(nGroupId, JSON.stringify(jsGorup));
						},
						get : function(groupId) {
							const nGroupId = groupId == undefined ? DEFAULT_ID_WORK : groupId;
							let str = $$buff.getItem(nGroupId);
							return str == undefined ? {__CK__:GROUP_ID_FIELD} : JSON.parse(str);
						},
						del : function(groupId) {
							const nGroupId = groupId == undefined ? DEFAULT_ID_WORK : groupId;
							$$buff.removeItem(groupId);
						},
						getIds : function() {
							return Array.from(Object.keys($$buff))
										.filter(c=>{
											let chk = false;
											try {
												chk = c != DEFAULT_ID_WORK && JSON.parse($$buff.getItem(c))["__CK__"] == GROUP_ID_FIELD;
											}catch(e) {
												chk = false;
											}
											return chk;
										});
						},
						name : "$data.storage.io.core.group"
					},

					records : {
						set : function(records, model, groupId) {
							const $group = $data.storage.io.core.group;
							
							let group = $group.get(groupId);
							group[model] = records;

							$group.set(group, groupId);
						},
						get : function(model, groupId) {
							const $group = $data.storage.io.core.group;
							
							let group = $group.get(groupId);
							let records = group[model];
							
							return records == undefined ? {} : records;
						},
						name : "$data.storage.io.core.records"
					},
					record : {
						set : function(key, record, model, groupId) {
							const $records = $data.storage.io.core.records;
							
							let records = $records.get(model, groupId);
							records[key] = record;
							
							$records.set(records, model, groupId);
						},
						del : function(key, model, groupId) {
							const $records = $data.storage.io.core.records;
							
							let fn_del = function(records, key) {
								let keys = Array.from(Object.keys(records));
								// child 삭제
								let childKeys = keys.filter(s => {return records[s].p_uuid == key;});
								childKeys.forEach(function(item, idx) {
									// TODO : 다른 Tab에서 항목을 사용중이라면 함께 삭제 하도록 해야 한다.
									fn_del(records, item);
								});
								delete records[key];
							}
							
							let records = $records.get(model, groupId);
							fn_del(records, key);
							$records.set(records, model, groupId);
						},
						delByIdx : function(index, model, groupId) {
							const $records = $data.storage.io.core.records;
							// TODO :: 추후 자식 노드가 있을 경우, 자식노드부터 삭제 해야 한다.
							let records = $records.get(model, groupId);
							let keys = Array.from(Object.keys(records));
							this.del(keys[index], model, groupId);
						},
						get : function(key, model, groupId) {
							const $records = $data.storage.io.core.records;
							
							let records = $records.get(model, groupId);
							return records[key];
						},
						getByUUID : function(uuid, model, groupId) {
							return this.get(uuid, model, groupId);
						},
						getByIdx : function(idx, model, groupId) {
							const $records = $data.storage.io.core.records;
							let records = $records.get(model, groupId);
							let keys = Array.from(Object.keys(records));
							return records[keys[idx]];
						},
						name : "$data.storage.io.core.record"
					},
					name : "$data.storage.io.core"
				},
				group : {
					/**
					 * 그룹 별 데이터를 관리 함.(저장/로드/삭제 등..)
					 */
					save : function(groupId) {
						const $group = $data.storage.io.core.group;
						$group.set($group.get(), groupId);
					},
					load : function(groupId) {
						// 특정 그룹ID를 선택 함.
						const $group = $data.storage.io.core.group;
						$group.set($group.get(groupId));
					},
					del : function(groupId) {
						const $group = $data.storage.io.core.group;
						return $group.del(groupId);
					},
					getIds : function() {
						const $group = $data.storage.io.core.group;
						return $group.getIds();
					},
					name : "$data.storage.io.group"
				},
				siteInfos : {
					/**
					 * 특정 Group에 저장된 Item 그룹을 관리 함.
					 */
					set : function(items, groupId) {
						const $records = $data.storage.io.core.records;
						const model = $data.storage.cons.key.siteInfo;
						$records.set(items, model, groupId);
					},
					get : function(groupId) {
						const $records = $data.storage.io.core.records;
						const model = $data.storage.cons.key.siteInfo;
						return $records.get(model, groupId);
					},
					name : "$data.storage.io.siteInfos"
				},
				siteInfo : {
					/**
					 * 하나의 Item(JSON Object)을 UUID 키로 관리 함.
					 */
					set : function(key, item, groupId) {
						const $record = $data.storage.io.core.record;
						const model = $data.storage.cons.key.siteInfo;
						$record.set(key, item, model, groupId);
					},
					del : function(key, groupId) {
						const $record = $data.storage.io.core.record;
						const model = $data.storage.cons.key.siteInfo;
						$record.del(key, model, groupId);
					},
					get : function(key, groupId) {
						const $record = $data.storage.io.core.record;
						const model = $data.storage.cons.key.siteInfo;
						return $record.get(key, model, groupId);
					},
					name : "$data.storage.io.siteInfo"
				},
				items : {
					/**
					 * 특정 Group에 저장된 Item 그룹을 관리 함.
					 */
					set : function(items, groupId) {
						const $records = $data.storage.io.core.records;
						const model = $data.storage.cons.key.item;
						$records.set(items, model, groupId);
					},
					get : function(groupId) {
						const $records = $data.storage.io.core.records;
						const model = $data.storage.cons.key.item;
						return $records.get(model, groupId);
					},
					name : "$data.storage.io.items"
				},
				item : {
					/**
					 * 하나의 Item(JSON Object)을 UUID 키로 관리 함.
					 */
					set : function(key, item, groupId) {
						const $record = $data.storage.io.core.record;
						const model = $data.storage.cons.key.item;
						$record.set(key, item, model, groupId);
					},
					del : function(key, groupId) {
						const $record = $data.storage.io.core.record;
						const model = $data.storage.cons.key.item;
						$record.del(key, model, groupId);
					},
					get : function(key, groupId) {
						const $record = $data.storage.io.core.record;
						const model = $data.storage.cons.key.item;
						return $record.get(key, model, groupId);
					},
					getByUUID : function(uuid, groupId) {
						return this.get(uuid, groupId);
					},
					getByIdx : function(idx, groupId) {
						const $record = $data.storage.io.core.record;
						const model = $data.storage.cons.key.item;
						return $record.getByIdx(idx, model, groupId);
					},
					getByCss : function(jsObject, groupId) {
						const records = $data.storage.io.core.records;
						const model = $data.storage.cons.key.item;
						const $util = $data.util;
						
						let items = records.get(model, groupId);
						let keys = Array.from(Object.keys(items));
						let idx = -1;
						
//						jsObject.absPath = $util.joinStr("body", jsObject.absPath, PATH_SEP_STR);
						let strPath = $util.joinStr(jsObject.absPath, jsObject.relativePath, PATH_SEP_STR);
						for(let i=0; i<keys.length; i++) {
							if(strPath == $util.getFullPath(items[keys[i]])) {
								idx = i;
								break;
							}
						}
						return idx>=0 ? items[keys[idx]] : undefined;
					},
					name : "$data.storage.io.item"
				},
				actions : {
					/**
					 * Action의 종류를 관리 함.
					 * 각 Action은 배열 목록으로 저장 됨.
					 */
					getIds : function(groupId) {
						const $records = $data.storage.io.core.records;
						const model = $data.storage.cons.key.action;
						let jsRecords = $records.get(model, groupId);
//						return Array.from(Object.keys(jsRecords));
						return Array.from(Object.keys(jsRecords))
						.filter(c=>{
							let chk = false;
							try {
								chk = c != DEFAULT_ID_WORK;
							}catch(e) {
								chk = false;
							}
							return chk;
						});
					},
					set : function(actions, actionId, groupId) {
						const $records = $data.storage.io.core.records;
						const model = $data.storage.cons.key.action;
						let jsRecords = $records.get(model, groupId);
						if(actionId == undefined) {
							actionId = DEFAULT_ID_WORK;
						}
						jsRecords[actionId] = actions;
						$records.set(jsRecords, model, groupId);
					},
					del : function(actionId, groupId) {
						const $records = $data.storage.io.core.records;
						const model = $data.storage.cons.key.action;
						let jsRecords = $records.get(model, groupId);
						if(actionId == undefined) {
							actionId = DEFAULT_ID_WORK;
						}
						delete jsRecords[actionId];
						$records.set(jsRecords, model, groupId);
					},
					get : function(actionId, groupId) {
						const $records = $data.storage.io.core.records;
						const model = $data.storage.cons.key.action;
						let jsRecords = $records.get(model, groupId);
						if(actionId == undefined) {
							actionId = DEFAULT_ID_WORK;
						}
						return jsRecords[actionId] == undefined ? [] : jsRecords[actionId];
					},
					name : "$data.storage.io.actions"
				},
				action : {
					/**
					 * Action ID에 해당하는 Action목록에 항목을 추가/삭제 하는 처리를 함.
					 */
					set : function(idx, action, actionId, groupId) {
						const $actions = $data.storage.io.actions;
						let actions = $actions.get(actionId, groupId);
						let newActions = actions.splice(0,idx).concat(action).concat(actions);
						$actions.set(newActions, actionId, groupId);
					},
					add : function(action, actionId, groupId) {
						const $actions = $data.storage.io.actions;
						let actions = $actions.get(actionId, groupId);
						actions.push(action);
						$actions.set(actions, actionId, groupId);
					},
					get : function(idx, actionId, groupId) {
						const $actions = $data.storage.io.actions;
						let actions = $actions.get(actionId, groupId);
						return actions[idx] == undefined ? {} : actions[idx];
					},
					del : function(idx, actionId, groupId) {
						const $actions = $data.storage.io.actions;
						let actions = $actions.get(actionId, groupId);
						
						let newActions = actions.splice(0,idx).concat(actions.splice(1));
						$actions.set(newActions, actionId, groupId);
					},
					name : "$data.storage.io.action"
				},
				views : {
					/**
					 * View의 종류를 관리 함.
					 * 각 View은 배열 목록으로 저장 됨.
					 */
					getIds : function(groupId) {
						const $records = $data.storage.io.core.records;
						const model = $data.storage.cons.key.view;
						let jsRecords = $records.get(model, groupId);
//						return Array.from(Object.keys(jsRecords));
						return Array.from(Object.keys(jsRecords))
						.filter(c=>{
							let chk = false;
							try {
								chk = c != DEFAULT_ID_WORK;
							}catch(e) {
								chk = false;
							}
							return chk;
						});
					},
					set : function(views, viewId, groupId) {
						const $records = $data.storage.io.core.records;
						const model = $data.storage.cons.key.view;
						let jsRecords = $records.get(model, groupId);
						if(viewId == undefined) {
							viewId = DEFAULT_ID_WORK;
						}
						jsRecords[viewId] = views;
						$records.set(jsRecords, model, groupId);
					},
					del : function(viewId, groupId) {
						const $records = $data.storage.io.core.records;
						const model = $data.storage.cons.key.view;
						let jsRecords = $records.get(model, groupId);
						if(viewId == undefined) {
							viewId = DEFAULT_ID_WORK;
						}
						delete jsRecords[viewId];
						$records.set(jsRecords, model, groupId);
					},
					get : function(viewId, groupId) {
						const $records = $data.storage.io.core.records;
						const model = $data.storage.cons.key.view;
						let jsRecords = $records.get(model, groupId);
						if(viewId == undefined) {
							viewId = DEFAULT_ID_WORK;
						}
						return jsRecords[viewId] == undefined ? [] : jsRecords[viewId];
					},
					name : "$data.storage.io.views"
				},
				view : {
					/**
					 * View ID에 해당하는 View목록에 항목을 추가/삭제 하는 처리를 함.
					 */
					set : function(idx, view, viewId, groupId) {
						const $views = $data.storage.io.views;
						let views = $views.get(viewId, groupId);
						let newViews = views.splice(0,idx).concat(view).concat(views);
						$views.set(newViews, viewId, groupId);
					},
					add : function(view, viewId, groupId) {
						const $views = $data.storage.io.views;
						let views = $views.get(viewId, groupId);
						views.push(view);
						$views.set(views, viewId, groupId);
					},
					get : function(idx, viewId, groupId) {
						const $views = $data.storage.io.views;
						let views = $views.get(viewId, groupId);
						return views[idx] == undefined ? {} : views[idx];
					},
					del : function(idx, viewId, groupId) {
						const $views = $data.storage.io.views;
						let views = $views.get(viewId, groupId);
						
						let newViews = views.splice(0,idx).concat(views.splice(1));
						$views.set(newViews, viewId, groupId);
					},
					name : "$data.storage.io.view"
				},
				name : "$data.storage.io"
			},

			name : "$data.storage"
		},
		util : {
			joinStr : function(p_path1, p_path2, sepStr) {
				return `${p_path1}${p_path1 == "" || p_path2 == "" ? "" : sepStr}${p_path2}`
			},
			getUUID : function() {
				let fn_s4 = function() {
					return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
				};
				// 중복 예방을 위해 존재하지 않는 UUID만을 탐색 한다.(중복될 일 거의... 없지만.....)
				return `${fn_s4()}${fn_s4()}-${fn_s4()}-${fn_s4()}-${fn_s4()}-${fn_s4()}${fn_s4()}${fn_s4()}`;
			},
			getFullPath : function(jsObject, groupId) {
				const $items = $data.storage.io.items;
				const $util = $data.util;
				const items = $items.get(groupId);
				
				let path = "";
				let uuid = jsObject.uuid == undefined ? "" : jsObject.uuid;
				
				do {
					if(items[uuid] == undefined) {
						break;
					}
					path = $util.joinStr(items[uuid].relativePath, path, PATH_SEP_STR);
					uuid = items[uuid].p_uuid == undefined ? "" : items[uuid].p_uuid;
				}while (uuid != "");
				
				return path;
			},
			name : "$data.util"
		},
		name : "$data"
	}; // eof $data

	
	$(document).ready(function() {
		$data.init();
	});
})(localStorage);