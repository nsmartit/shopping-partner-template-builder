(function(){
	
	$page2 = {
		init : function() {
			$page2.ui.init();
			$page2.event.init();
		},
		ui : {
			init : function() {
				$page2.actionGroup.ui.init();
				$page2.actionList.ui.init();
			},
			name : "$page2.ui"
		},
		event : {
			init : function() {
				$page2.actionGroup.event.init();
				$page2.actionList.event.init();
			},
			onRecordAction : function(jsObject) {
				$page2.actionList.ui.redraw();
			},
			name : "$page2.event"
		},
		msgSend : {
			contents : {
				actionPrev : function(jsObject, callback) {
					$("#tab2 div[data-id=actionPrevResult]").text("");

					chrome.tabs.query({
						active : true,
						currentWindow : true
					}, function(tabs) {
						try{
//							chrome.tabs.reload(tabs[0].id);
							var updateProperties = { 'active': true };
							chrome.tabs.update(tabs[0].id, updateProperties, (tab) => { });
						}
						catch(e) {
							$message.alert.show(e);
						}
					});
					
					$message.msg.contents.send({
						action : "actionPrev",
						data : jsObject
					}, function(response) {
						
						$("#tab2 div[data-id=actionPrevResult]").text(response.value);
						if (callback) {
							callback(response);
						}
					});

				},

				name : "$page2.msgSend.contents"
			},
			name : "$page2.msgSend"
		},
		actionGroup : {
			ui : {
				init : function() {
					const conEl = $("#tab2 select#action");
					let addId = function(actionId) {
						conEl.append($(`<option data-key="${actionId}">${actionId}</option>`));
					}
					
					conEl.empty()
					$dataAction.group.getIds().forEach(function(item, idx) {
						addId(item);
					});
					
					$page2.actionGroup.ui.setId($layout.group.data.json.actionId);
				},
				getId : function() {
					const conEl = $("#tab2 select#action");
					let actionId = conEl.find("option:selected").data("key");
					return actionId;
				},
				setId : function(newId) {
					const conEl = $("#tab2 select#action");
					
					if(newId == undefined) {
						conEl.find(`option:not([data-key]:eq(0))`).prop("selected", true);
					} else {
						
						if(conEl.find(`option[data-key=${newId}]`).length == 0) {
							alert(`액션 [${newId}]을 찾을 수 없습니다.`);
							newId = conEl.find(`option[data-key]:eq(0)`).data("key");
						} else {
							conEl.find(`option[data-key=${newId}]`).prop("selected", true);
						}
					}
					$layout.group.data.json.actionId = newId;
					$layout.group.data.save();
				},

				name : "$page2.actionGroup.ui"
			},
			event : {
				init : function() {
					$("#actionSave").off("click").on("click", function(){
						
						let newName = window.prompt("저장할 Action명을 입력 하세요.", $("#action").val());
						let ids = $dataAction.group.getIds();
						let newIdx = -1;
						for(let i=0; i<ids.length; i++) {
							if(ids[i] == newName) {
								newIdx = i;
								break;
							}
						}
						if((newIdx < 0) || confirm("기존 항목에 덮어 쓰시겠습니까?")) {
							let actions = $actions.get();
							$dataAction.group.save(actions, newName);
							
							$page2.actionGroup.ui.init();
							$page2.actionGroup.event.init();
							$page2.actionGroup.ui.setId(newName);
						}

					});
					
					$("#actionReset").off("click").on("click", function(){
						const $group = $data.storage.io.group;
						const conEl = $("#tab2 select#action");
						// 그룹을 변경할때 이벤트
						let id = conEl.find("option:selected").data("key");
						if(id == undefined) {
							$dataAction.group.del(undefined);
						} else {
							if(confirm(`[${id}]액션을 불러 올까요?\n* 현재 작업중인 내용은 삭제 됩니다.`)) {
								$dataAction.group.load(id);
							} else {
								$page2.actionGroup.ui.setId($layout.group.data.json.actionId);
							}
						}
						$page2.init();
					});
					$("#actionDel").off("click").on("click", function(){
						const $group = $data.storage.io.group;
						const conEl = $("#tab2 select#action");
						let id = conEl.find("option:selected").data("key");
						
						if(id != undefined && confirm(`[${id}] 항목을 삭제 하시겠습니까?`)) {
							// 현재 선택한 ID를 삭제 함.
							$dataAction.group.del(id);
							// 첫항목으로 셋팅 함.
							let newId = $dataAction.group.getIds()[0];
							if(newId == undefined) {
								// 최종 항목일 경우... 
								$dataAction.group.del(undefined);
							} else {
								// 항목이 남았을 경우..
								$dataAction.group.load(newId);
							}
							$page2.actionGroup.ui.setId(newId);
							$page2.init();
							alert("삭제 되었습니다.(첫 항목으로 초기화 됩니다.)");
						} 
					});
					$("#action").off("change").on("change", function(){
						// 그룹을 변경할때 이벤트
						let actionId = $(this).find("option:selected").data("key");
						if(actionId != undefined) {
							if(confirm(`[${actionId}] 액션을 불러 올까요?(현재 작업중인 내용은 삭제 됩니다.)`)) {
								$dataAction.group.load(actionId);
								$page2.actionGroup.ui.setId(actionId);
								$page2.init();
							}
						}
					});
					$("#actionPlay").off("click").on("click", function(){
						// 액션 실행할 목록을 생성 함.
						let currentData = localStorage.getItem("DEF");
						$page2.msgSend.contents.actionPrev(JSON.parse(currentData));
						
					});
				},
				name : "$page2.actionGroup.event"
			},
			name : "$page2.actionGroup"
		},
		actionList : {
			ui : {
				init : function() {
					this.redraw();
				},
				displayInfo : function(jsObject) {
					let fnGetItemAliasLists = function() {
						/* Item 항목 중 alias가 있는 항목 목록을 조회 함. */
						let aliases = $dataItem.getAliases();
						let aliasItems = aliases.filter(c=>{return c.item.alias != undefined});
						let uuids = [];
						aliasItems.forEach(function(aliasItem, idx){
							uuids.push(aliasItem.item.alias);
						});

						return uuids;
					};
					let fnGetViewLists = function() {
						/* View 항목 목록을 조회 함. */
						return $dataView.group.getIds();
					};
					let fnRefreshAcValue = function(texts, value) {
						let conEl = $(`#tab2 select[data-id=actionConditionValue]`);
						conEl.empty();
						$(texts).each(function(idx, text) {
							if(text == value) {
								conEl.append($(`<option value="${text}" selected="selected">${text}</option>`));
							} else {
								conEl.append($(`<option value="${text}">${text}</option>`));
							}
						});
					};
					
					$(`#tab2 input[data-id=uuid]`).val(jsObject.uuid);
					$(`#tab2 input[name=tab2-action][value=${jsObject.action}]`).prop("checked", true);
					$(`#tab2 input[name=tab2-actionConditionType][value=${jsObject.condType}]`).prop("checked", true);
//					$(`#tab2 input[data-id=actionConditionValue]`).val(jsObject.condVal);

					$("#tab2 div.tr[data-id=row-actionConditionType] div.td").hide();
					$("#tab2 div.tr[data-id=row-actionConditionValue] div.td").hide();
					switch(jsObject.action) {
					case "startLoop": {
						$("#tab2 div.tr[data-id=row-actionConditionType] div.td").show();
						$("#tab2 div.tr[data-id=row-actionConditionValue] div.td").show();
	
						$("#tab2 div.td[data-id=lb-actionConditionValue]").text("반복값");

						fnRefreshAcValue(fnGetItemAliasLists(), jsObject.condVal);
					}
					break;
					case "read":{
						$("#tab2 div.tr[data-id=row-actionConditionValue] div.td").show();

						$("#tab2 div.td[data-id=lb-actionConditionValue]").text("추출Set");
						
						fnRefreshAcValue(fnGetViewLists(), jsObject.condVal);
					}
					break;
					}
				},
				redraw : function() {
					const conEl = $("#tab2 [data-id=list_actions]");
					const TRM = "    ";
					let term = "";
					
					let actions = $dataAction.group.get();
					let aliases = $dataItem.getAliases();
					conEl.empty();
					let delIdxs = [];
					$(actions).each(function(idx, item) {
						
						switch(item.action) {
						case "click": 
							alias = aliases.filter(c=>{return c.key == item.uuid});
							if(alias.length == 0) {
								// item 목록에서 삭제 됨.
								delIdxs.push(idx);
							} else {
								conEl.append($(`<div class="sel click" data-action="${item.action}" data-key="${item.uuid}"><pre>${term}${alias[0].alias.trim()}</pre></div>`));
							}
							break;
						case "startLoop": 
							conEl.append($(`<div class="sel action" data-action="${item.action}" data-key="${item.uuid}"><pre>${term}반복 시작</pre></div>`));
							term = term + TRM;
							break;
						case "endLoop": 
							term = term.replace(TRM, "");
							conEl.append($(`<div class="sel action" data-action="${item.action}" data-key="${item.uuid}"><pre>${term}반복 종료</pre></div>`));
							break;
						case "read": 
//							term = term.replace(TRM, "");
							conEl.append($(`<div class="sel view" data-action="${item.action}" data-key="${item.uuid}"><pre>${term}값 추출</pre></div>`));
							break;
						}
						
					});

					for(let i=delIdxs.length-1; i>=0; i--) {
						$dataAction.item.del(delIdxs[i]);
					}
				},
				name : "$page2.actionList.ui"
			},
			event : {
				init : function() {
					let conEl = $("#tab2 [data-id=list_actions]");
					
					let item_onDblClick = function(item) {
						// Click Event 항목 노출..
						let uuid = $(item).data("key");
//						let idx = $dataAction.item.toIdx(uuid);
						let action = item.data("action");
						
						$(`div.sel pre.select`).removeClass("select");
						$(`div.sel[data-key=${uuid}] pre`).addClass("select");
						$page1.ui.selectItem(uuid);
						$tab.selectTab(0);
						
						$page2.actionList.ui.displayInfo({});
					};
					let view_onDblClick = function(item) {
						// Click Event 항목 노출..
						let uuid = $(item).data("key");
						let idx = $dataView.item.toIdx(uuid);
						let action = item.data("action");
						
						$(`div.sel pre.select`).removeClass("select");
						$(`div.sel[data-key=${uuid}] pre`).addClass("select");
						$page1.ui.selectItem({});
						$page3.ui.selectItem(uuid);
						$page2.actionList.ui.displayInfo($dataAction.item.get(idx));
						$tab.selectTab(2);
						
					};
					let item_onClick = function(item) {
						// Loop 항목을 클릭 했을때 상세 정보 노출
						
						let uuid = $(item).data("key");
						let idx = $dataAction.item.toIdx(uuid);
						let action = item.data("action");
						
						$(`div.sel pre.select`).removeClass("select");
						$(`div.sel[data-key=${uuid}] pre`).addClass("select");
						$page1.ui.displayInfo({});
						$page2.actionList.ui.displayInfo($dataAction.item.get(idx));
						
					};
					let fnModifyInfo = function() {
						const $ui = $page1.ui;

						// 변경된 값을 list 항목에 저장 함.
						let uuid = $("#tab2 input[data-id=uuid]").val();
						let idx = $dataAction.item.toIdx(uuid);
						
						let jsObject = $dataAction.item.get(idx);
						jsObject.uuid = uuid;
						
						jsObject.action = $("#tab2 input[name=tab2-action]:checked").val();
						jsObject.condType = $("#tab2 input[name=tab2-actionConditionType]:checked").val();
						jsObject.condVal = $("#tab2 select[data-id=actionConditionValue]").val();
						
						$dataAction.item.set(idx, jsObject);
						
						// Item의 정보를 갱신 함.
//						$ui.refreshItems();
						$tab.tabs.ui.init();
						
					};
					conEl.off("dblclick", "div.sel.click").on("dblclick", "div.sel.click", function(){
						item_onDblClick($(this));
					});
					conEl.off("dblclick", "div.sel.view").on("dblclick", "div.sel.view", function(){
						view_onDblClick($(this));
					});
					conEl.off("click", "div.sel.action,div.sel.view").on("click", "div.sel.action,div.sel.view", function(){
						item_onClick($(this));
					});
					$("#tab2 .attInfo").off("change", "input").on("change", "input,select", fnModifyInfo);
				},
				name : "$page2.actionList.event"
			},
			name : "$page2.actionList"
		},
		contextMenu : {
			deleteItem : function(orgUIEl) {
				let idx = $(orgUIEl).parent().index();
				$dataAction.item.del(idx);
				$page2.actionList.ui.displayInfo({});
				$page2.actionList.ui.redraw();
			},
			acLoopStart : function(orgUIEl) {
				let jsObject = {
					action : "startLoop",
					uuid : $page2.getActionUUID("sL"),
					alias : "루프 시작"
				};
				let idx = $(orgUIEl).parent().index();
				$dataAction.item.add(idx, jsObject);
				$tab.tabs.event.onRecordAction(jsObject);
			},
			acLoopEnd : function(orgUIEl) {
				let jsObject = {
					action : "endLoop",
					uuid : $page2.getActionUUID("eL"),
					alias : "루프 종료"
				};
				let idx = $(orgUIEl).parent().index();
				$dataAction.item.add(idx+1, jsObject);
				$tab.tabs.event.onRecordAction(jsObject);
			},
			acRead : function(orgUIEl) {
				let jsObject = {
						action : "read",
						uuid : $page2.getActionUUID("eL"),
						alias : "루프 종료"
				};
				let idx = $(orgUIEl).parent().index();
				$dataAction.item.add(idx+1, jsObject);
				$tab.tabs.event.onRecordAction(jsObject);
			},
			name : "$page2.contextMenu"
		},

		getActionUUID : function(prefix) {
			const fn_isDuplicateUUID = function(uuid) {
				return $dataAction.group.getIds().filter(id=>{return id == uuid}).length>0;
				
			};
			let fn_getUUID = function() {
				// 중복 예방을 위해 존재하지 않는 UUID만을 탐색 한다.(중복될 일 거의... 없지만.....)
				let uuid = "";
				do {
					uuid = `${prefix}-${$data.util.getUUID()}`;
				} while (fn_isDuplicateUUID(uuid));
				return uuid;
			};
			return fn_getUUID();
		},

		name : "$page2"
	}; // eof page2

// 	layout에서 UI 구성 후 호출 함.
//	$(document).ready(function(){
//		$page2.init();
//	});
})();
