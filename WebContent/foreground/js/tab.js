(function() {

	$tab = {
			
		init : function() { // call by $layout.init()!!
			
			// tab click event binding
			 $(`ul#tab>li>a`).off("click").on("click", function(){
				 $layout.group.data.json.tabIdx = $(this).parent().index();
				 $layout.group.data.save();
				location.href = $(this).data("link");
			});

			let tabIdx = $layout.group.data.json.tabIdx;
			this.selectTab(tabIdx == undefined ? 0 : tabIdx);
		},
		selectTab : function(idx) {
			let tab = $(`ul#tab>li:eq(${idx})>a`);
			if(tab.length>0) {
				tab.click();
			}
		},
		tabs : {
			items : [ $page1, $page2, $page3 ],
			ui : {
				init : function() {
					$tab.tabs.items.forEach(function(item){
						if(item.init != undefined) {
							item.init();
						}
					});
				},
				name : "$tab.tabs.ui"
			},
			/**
			 * 각 탭에 이벤트를 전달 시킴
			 */
			event : {
				onContentsSelect : function(cssSelectorArr, stat, strElTxt) {
					$($tab.tabs.items).each(function() {
						if(this.event.onContentsSelect != undefined) {
							this.event.onContentsSelect(cssSelectorArr, stat, strElTxt);
						}
					})
				},
				onRecordView : function(jsObject) {
					$($tab.tabs.items).each(function() {
						if(this.event.onRecordView != undefined) {
							this.event.onRecordView(jsObject);
						}
					})
				},
				onRecordAction : function(jsObject) {
					$($tab.tabs.items).each(function() {
						if(this.event.onRecordAction != undefined) {
							this.event.onRecordAction(jsObject);
						}
					})
				},
				name : "$tab.tabs.event"
			},
			name : "$tab.tabs"
		},
		name : "$tab"
	}; // eof tab

})();
// init call
