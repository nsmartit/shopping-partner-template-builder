(function(id) {
// "use strict";
	
	$contextMenu = {
		_opts : {
			classes : {
				ac_show 	: "show"
			,	ac_hide 	: "hide"

			}
		},
		_target : undefined,
		init : function(id) {
			if(id == undefined) {
				id = "_0mzbd0_";
			}
			$contextMenu._opts.classes.id = id;
			$contextMenu._opts.classes.menu = `${id}-menu`;
			$contextMenu._opts.classes.menu_opts = `${id}-menu-options_`;
			$contextMenu._opts.classes.menu_opt = `${id}-menu-option_`;
			
			
			$contextMenu.ui.initUI();
			$contextMenu.event.bindEvent();
		},
		ui : {
			initUI : function() {
				const getMenuText = (items) => {
					let tag = ``;
					tag = tag + (`  <ul class="${$contextMenu._opts.classes.menu_opts}">`);
					items.forEach(function(item){
						tag = tag + (`    <li class="${$contextMenu._opts.classes.menu_opt}" data-page="${item.pageIdx}" data-key="${item.key}">${item.text}</li>`);
						if(item.sub != undefined) {
							tag = tag + getMenuText(item.sub);
						}
					});
					tag = tag + (`  </ul>`);
					return tag;
				};
				const genMenu = ({items}) => {
					let tag = ``;
					tag = tag + (`<div class="${$contextMenu._opts.classes.menu}">`);
					tag = tag + getMenuText(items);
					tag = tag + (`</div>`);
					
					$("body").append(tag);
				};

				let menu_acLoop = [
					{key:"acLoopStart", 	pageIdx:1, 	text:"반복 시작"},
					{key:"acLoopEnd", 		pageIdx:1, 	text:"반복 종료"}
				];
				let menu = {
						  items : [
								// --------------------- PAGE 0 -----------------
							  	{key:"deleteItem", 	pageIdx:0, 	text:"삭제"},
								
								// --------------------- PAGE 1 -----------------
								{key:"deleteItem", 	pageIdx:1, 	text:"삭제"},
								{key:"acLoopStart", pageIdx:1, 	text:"반복 시작"},
								{key:"acLoopEnd", 	pageIdx:1, 	text:"반복 종료"},
								{key:"acRead", 		pageIdx:1, 	text:"값 추출"},
								// --------------------- PAGE 1 -----------------
								{key:"deleteItem", 	pageIdx:2, 	text:"삭제"},
							]
				};
				genMenu(menu);
			},
			resetMenuText : function() {
				/**
				 * ContextMenu의 Text를 상태에 맞게 노출 함.
				 **/
				$(`.${$contextMenu._opts.classes.menu} .${$contextMenu._opts.classes.menu_opt}`).each((index, item) => {
//					let key = $(item).data("key");
					if($layout.group.data.json.tabIdx == $(item).data("page")) {
						$(item).show();
					} else {
						$(item).hide();
					}
				});
			},
			getMenuObject : function() {
				return document.querySelector(`.${$contextMenu._opts.classes.menu}`);
			},
			toggleMenu : function(command) {
				const menu = this.getMenuObject();
				let menuVisible = (command === $contextMenu._opts.classes.ac_show);
				menu.style.display = menuVisible ? "block" : "none";
			},
			isShow : function() {
				const menu = this.getMenuObject();
				return menu.style.display == "block";
			},
			show : function(left, top, targetEl) {
				const menu = this.getMenuObject();
				if(!this.isShow()) {
					// Refresh Menu Item Text
					this.resetMenuText();
					// Set TargetEl
					$contextMenu._target = targetEl;
					// Show Menu
					menu.style.left = `${left}px`;
					menu.style.top = `${top}px`;
					this.toggleMenu($contextMenu._opts.classes.ac_show);
				}
			},
			hide : function() {
				if(this.isShow()) {
					this.toggleMenu($contextMenu._opts.classes.ac_hide);
					$contextMenu._target = undefined;
				}
			},
			name : "ui"
		}, 
		event : {
			bindEvent : function() {
				const onHideMenu = function(e) {
					let isShow = $contextMenu.ui.isShow(); 
					if(isShow) {
						e.preventDefault();
						$contextMenu.ui.hide();
					}
					return !isShow;
				};
				const onClickMenuItem = function(e) {
					let isShow = $contextMenu.ui.isShow(); 
					let orgEl = $contextMenu._target;
					if(isShow) {
						e.preventDefault();
						$contextMenu.ui.hide();
						$contextMenu.event.menuSelect(e.target, orgEl);
					}
					
					return !isShow;
				};

				// DEFAULT EVENT LISTENER //
				window.addEventListener("click", onHideMenu);
				// JQUERY EVENT LISTENER :: ui 이벤트에서 이벤트 전파를 취소 하면 window의 이벤트는 동작하지 않는다. //
				$(`[class^=${$contextMenu._opts.classes.id}]`)
					.off("click")
					.on("click", onClickMenuItem)
					;

				window.addEventListener("contextmenu", e => {
					$(e.target).parent().click();
					e.preventDefault();
					$contextMenu.ui.show(e.pageX, e.pageY, e.srcElement);
					return false;
				});
			},
			menuSelect : function(menuItem, orgUIEl) {
				/**
				 * 팝업 메뉴를 선택 함.
				 * 
				 * @param menuItem
				 *            선택한 팝업메뉴의 항목
				 * @param orgUIEl
				 *            팝업을 열때 선택한 화면의 Element
				 */
				let retval = false;
				// context menu 선택 함.
				let jqMenuItem = $(menuItem);

				// 여기에 옵션에 따른 처리를 하도록 기능 추가 함.
				let key = jqMenuItem.data("key");

				let tabItem = $tab.tabs.items[$layout.group.data.json.tabIdx];
				let fnCallback = undefined;
				switch(key) {
				case "deleteItem":
					fnCallback = tabItem.contextMenu.deleteItem;
					break;
				case "acLoopStart":
					// "반복 시작"
					fnCallback = tabItem.contextMenu.acLoopStart;
					break;
				case "acLoopEnd":
					// "반복 종료"
					fnCallback = tabItem.contextMenu.acLoopEnd;
					break;
				case "acRead":
					// "반복 종료"
					fnCallback = tabItem.contextMenu.acRead;
					break;

				}
				
				if(fnCallback != undefined) {
					fnCallback(orgUIEl);
				}
				
				return retval;
			},
			name : "event"
		}
		
	};

	$(document).ready(function(){
		$contextMenu.init();
	});
})();