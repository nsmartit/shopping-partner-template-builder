(function() {
	$dataSiteInfo = {
		init : function() {
		},
		url : {
			key : "url",
			get : function() {
				const $siteInfo = $data.storage.io.siteInfo;
				let urls = $siteInfo.get(this.key);
				return urls == undefined ? [] : urls;
			},
			add : function(url) {
				const $siteInfo = $data.storage.io.siteInfo;
				let urls = this.get(this.key);
				urls.push(url);
				$siteInfo.set(this.key, urls);
			},
			
			del : function(site){
				const $siteInfo = $data.storage.io.siteInfo;
				let urls = this.get(this.key);
				$siteInfo.set(this.key, urls.filter(s=>{return s!=site}));
			},
			clear : function() {
				const $siteInfo = $data.storage.io.siteInfo;
				$siteInfo.set(this.key, []);
			},
			name : "$dataSiteInfo.url"
		},
		name : "$dataSiteInfo"
	}; // eof $data
	
	$(document).ready(function() {
		$dataSiteInfo.init();
	});
	
})();