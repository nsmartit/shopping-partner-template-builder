(function() {

	$layout = {
		init : function() {
			$layout.group.data.load();
			$tab.init();
			
			$([ "page1.html", "page2.html", "page3.html"]).each(function(i) {
				$.ajax({
					url : this,
					success : function(result) {
						$(`article:eq(${i})>div`).html(result);
						$(document).ready(function(){
							let item = $tab.tabs.items.length>i ? $tab.tabs.items[i] : undefined;
							if(item != undefined) {
								item.init();
							}
						});
					}
				});
			});
			
			$layout.group.init();
			$layout.siteInfo.init();
		},
		event : {
			init : function() {
				$group = $layout.group;
			},
			name : "$layout.event"
		},
		group : {
			init : function() {
				$layout.group.ui.init();
				$layout.group.event.init();
			},
			data : {
				json : {
					tabIdx : undefined,
					groupId : undefined,
					actionId : undefined,
					name : "$layout.group.data.json"
				},
				storage : {
					key : $data.storage.cons.key.layout,
					read : function() {
						const key = $layout.group.data.storage.key;
						let str = localStorage.getItem(key);
						if(str == undefined || str == "") {
							str = "{}";
						}
						return JSON.parse(str);
					},
					store : function(jsObj) {
						const key = $layout.group.data.storage.key;
						localStorage.setItem(key, JSON.stringify(jsObj));
					},
					name : "$layout.group.data.storage"
				},
				save : function() {
					const $storage = $layout.group.data.storage;
					let jsObj = $storage.read();
					
					jsObj["tabIdx"] = $layout.group.data.json.tabIdx;
					jsObj["groupId"] = $layout.group.data.json.groupId;
					jsObj["viewId"] = $layout.group.data.json.viewId;
					jsObj["actionId"] = $layout.group.data.json.actionId;

					$storage.store(jsObj);
				},
				load : function() {
					// tab이 가장 빠르므로, tab load시점에 호출 하도록 함.
					
					const $storage = $layout.group.data.storage;
					let jsObj = $storage.read();
					
					$layout.group.data.json.tabIdx = jsObj["tabIdx"];
					$layout.group.data.json.groupId = jsObj["groupId"];
					$layout.group.data.json.viewId = jsObj["viewId"];
					$layout.group.data.json.actionId = jsObj["actionId"];
				},
				name : "$layout.group.data"
			},
			ui : {
				init : function() {
					const $group = $data.storage.io.group;
					let addId = function(groupId) {
						$("#group").append($(`<option data-key="${groupId}">${groupId}</option>`));
					}
					
					let ids = $group.getIds();
					$("#group").empty();
					for(let i=0; i<ids.length; i++) {
						addId(ids[i]);
					}
					
//					$layout.group.data.load();
					$layout.group.ui.setId($layout.group.data.json.groupId);
					
				},
				setId : function(groupId) {

					if(groupId == undefined) {
						$("#group").find(`option:not([data-key]:eq(0))`).prop("selected", true);
					} else {
						if($("#group").find(`option[data-key=${groupId}]`).length == 0) {
							alert(`그룹 [${groupId}]을 찾을 수 없습니다.`);
						} else {
							$("#group").find(`option[data-key=${groupId}]`).prop("selected", true);
						}
					}
					$layout.group.data.json.groupId = groupId;
					$layout.group.data.save();
				},
				getId : function() {
					let groupId = $("#group").find("option:selected").data("key");
					return groupId;
				},
				name : "$layout.group.ui"
			},
			event : {
				init : function() {
					$("#groupSave").off("click").on("click", function(){

						const $group = $data.storage.io.group;
						var $groupId = $data.storage.io.core.group.get("LAYOUT_DATA").groupId;
						if($groupId == null) {
							$groupId = $("#group").val();
						}
						let newGroupName = window.prompt("저장할 템플릿(그룹)명을 입력 하세요.", $groupId == null ? "" : $groupId);
						let groupIds = $group.getIds();
						let newIdx = -1;
						for(let i=0; i<groupIds.length; i++) {
							if(groupIds[i] == newGroupName) {
								newIdx = i;
								break;
							}
						}
						
						if((newIdx < 0) || confirm("기존 항목에 덮어 쓰시겠습니까?")) {
							$group.save(newGroupName);
							
							$layout.group.ui.init();
							$layout.event.init();
							$layout.group.ui.setId(newGroupName);
						}

					});
					
					$("#groupReset").off("click").on("click", function(){
						const $group = $data.storage.io.group;
						// 그룹을 변경할때 이벤트
						let groupId = $("#group").find("option:selected").data("key");
						if(groupId == undefined) {
						} else {
							if(confirm(`[${groupId}] 그룹을 불러 올까요?\n* 현재 작업중인 내용은 삭제 됩니다.`)) {
								$group.load(groupId);
								$layout.group.ui.setId(groupId);
								$tab.tabs.ui.init();
								

							} else {
								$layout.group.ui.setId($layout.group.data.json.groupId);
							}
						}
					});
					$("#groupDel").off("click").on("click", function(){
						const $group = $data.storage.io.group;
						let groupId = $("#group").find("option:selected").data("key");
						if(groupId == undefined) {
							$group.del(groupId);
							
							$layout.group.ui.init();
							$layout.event.init();
							$layout.group.ui.setId($layout.group.ui.getId());
						}
						else if(confirm(`[${groupId}] 항목을 삭제 하시겠습니까?`)) {
							$group.del(groupId);
							
							$layout.group.ui.init();
							$layout.event.init();
							$layout.group.ui.setId($layout.group.ui.getId());
							alert("삭제 되었습니다.(첫 항목으로 초기화 됩니다.)");
						} 
					});
					$("#group").off("change").on("change", function(){
						const $group = $data.storage.io.group;
						// 그룹을 변경할때 이벤트
						let groupId = $(this).find("option:selected").data("key");
						if(groupId == undefined) {
						} else {
							if(confirm(`[${groupId}] 그룹을 불러 올까요?(현재 작업중인 내용은 삭제 됩니다.)`)) {
								$group.load(groupId);
								$layout.group.ui.setId(groupId);
								$tab.tabs.ui.init();
								
							} else {
								$layout.group.ui.setId($layout.group.data.json.groupId);
							}
						}
					});
	
				},
				name : "$layout.group.event"
			},
			name : "$layout.group"
		},
		siteInfo : {
			init : function() {
				$layout.siteInfo.ui.init();
				$layout.siteInfo.event.init();
			},
			data : {
				json : {
					site : [],
					name : "$layout.siteInfo.data.json"
				},
				storage : {
					key : "INFO_DATA",
					read : function() {
						const key = $layout.group.data.storage.key;
						let str = localStorage.getItem(key);
						if(str == undefined || str == "") {
							str = "{}";
						}
						return JSON.parse(str);
					},
					store : function(jsObj) {
						const key = $layout.group.data.storage.key;
						localStorage.setItem(key, JSON.stringify(jsObj));
					},
					name : "$layout.siteInfo.data.storage"
				},
				save : function() {
					const $storage = $layout.group.data.storage;
					let jsObj = $storage.read();
					
					jsObj["site"] = $layout.group.data.json.tabIdx;
					jsObj["groupId"] = $layout.group.data.json.groupId;
					jsObj["viewId"] = $layout.group.data.json.viewId;
					jsObj["actionId"] = $layout.group.data.json.actionId;

					$storage.store(jsObj);
				},
				load : function() {
					// tab이 가장 빠르므로, tab load시점에 호출 하도록 함.
					
					const $storage = $layout.group.data.storage;
					let jsObj = $storage.read();
					
					$layout.group.data.json.tabIdx = jsObj["tabIdx"];
					$layout.group.data.json.groupId = jsObj["groupId"];
					$layout.group.data.json.viewId = jsObj["viewId"];
					$layout.group.data.json.actionId = jsObj["actionId"];
				},
				name : "$layout.siteInfo.data"
			},
			ui : {
				init : function() {
					$layout.siteInfo.ui.reload();
				},
				reload : function() {
					const conEl = $("div[data-id=list_sites]");
					conEl.empty();
					$dataSiteInfo.url.get().forEach((item,idx)=>{conEl.append(`<div data-url="${item}">${item}</div>`)});
				},
				name : "$layout.siteInfo.ui"
			},
			event : {
				init : function() {
					$("#siteSave").off("click").on("click", function(){
						
						let url = $("input#url").val();
						if(url == "") {
							alert("템플릿을 적용할 사이트 정보를 입력 해 주세요.");
							$("input#url").focus();
						} else if($dataSiteInfo.url.get().filter(s=>{return s==url}).length>0) {
							alert("이미 등록된 URL 입니다.");
							$(`div[data-id=list_sites] div.warn`).removeClass("warn");
							$(Array.from($(`div[data-id=list_sites] div`)).filter(el=>{return $(el).data("url") == url})).addClass("warn");
						}else{
							if(confirm(`[${url}]을 저장 하시겠습니까?`)) {
								if($dataSiteInfo.url.get().length == 0 || confirm(`기존에 등록된 주소가 있습니다.\n변경 하시겠습니까?`)) {
									$dataSiteInfo.url.clear();
									$dataSiteInfo.url.add(url);
									$layout.siteInfo.ui.reload();
								}
							}
						}
					});
					$("#siteDel").off("click").on("click", function(){
						let url = $("input#url").val();
						if(url != "" && confirm(`[${url}]을 삭제 하시겠습니까?`)) {
							$dataSiteInfo.url.del(url);
							$layout.siteInfo.ui.reload();
						}
					});
					$("div[data-id=list_sites]").off("click", "div").on("click", "div", function(){
						$("input#url").val($(this).data("url"));
						$(`div[data-id=list_sites] div.warn`).removeClass("warn");
						$(`div[data-id=list_sites] div.sel`).removeClass("sel");
						$(this).addClass("sel");
					});
					
					$("#svrSend").off("click").on("click", function(){
						/* 현재 localStorage의 전체 내용을 서버에 전송 시킴 */
						if(confirm("현재 작업 사항을 서버에 저장하시겠습니까?")) {
							$data.storage.synch.save();
						}
					});
					$("#svrRead").off("click").on("click", function(){
						/* 서버에 저장된 내용을 읽어서 localStorage를 갱신 시킴 */
						if(confirm("서버에서 작업사항을 불러오시겠습니까?\n(현재 작업중인 내용은 모두 초기화 됩니다.)")) {
//							$net.post.send("http://test/test/read", sendData, function(data){
//								let dec = JSON.parse(LZString.decompressFromEncodedURIComponent(data.data))
//							});
							$data.storage.synch.load();
						}
					});
					$("#groupBuild").off("click").on("click", function(){
						/* 현재 그룹의 데이터를 빌드하여 배포 함. */
						const $groupId = $data.storage.io.core.group.get("LAYOUT_DATA").groupId;
						if($groupId == undefined || "" == $groupId) {
							alert("선택된 그룹이 없습니다.\n배포할 그룹을 선택하세요.");
							return false;
						} else if(!confirm(`[${$groupId}]의 데이터를 배포하시겠습니까?\n(배포 전에 "저장"을 해야 변경사항이 반영 됩니다.)`)) {
							return false;
						} else {
							$data.storage.synch.build($groupId);
						}
					});
				},
				name : "$layout.siteInfo.event"
			},
			name : "$layout.siteInfo"
		},
		name : "$layout"
	}; // eof layout

	$(document).ready(function() {
		$layout.init();
	});

})();
