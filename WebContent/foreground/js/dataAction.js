(function() {
	$dataAction = {
		init : function() {
		},
		group : {
			getIds : function(groupId) {
				$actions = $data.storage.io.actions;
				return $actions.getIds(groupId);
			},
			load : function(actionId) {
				const $actions = $data.storage.io.actions;
				let actions = $actions.get(actionId);
				$actions.set(actions);
			},
			save : function(actions, actionId) {
				const $actions = $data.storage.io.actions;
				$actions.set(actions, actionId);
			},
			get : function(actionId, groupId) {
				$actions = $data.storage.io.actions;
				return $actions.get(actionId, groupId);
			},
			del : function(id){
				const $actions = $data.storage.io.actions;
				$actions.del(id)
			},
			name : "$dataAction.group"
		},
		item : {
			toIdx : function(uuid, actionId, groupId) {
				const $actions = $data.storage.io.actions;
				let actions = $actions.get(actionId, groupId);
				let idx = -1;
				for(let i=0; i<actions.length; i++) {
					if(actions[i].uuid == uuid) {
						idx = i;
						break;
					}
				}
				return idx;
			},
			push : function(action, actionId, groupId) {
				const $action = $data.storage.io.action;
				$action.add(action, actionId, groupId); 
			},
			add : function(idx, action, actionId, groupId) {
				const $actions = $data.storage.io.actions;
				let actions = $actions.get(actionId, groupId);
				
				let newActions = actions.splice(0,idx)
								.concat(action)
								.concat(actions);
				$actions.set(newActions, actionId, groupId);
			},
			set : function(idx, action, actionId, groupId) {
				const $actions = $data.storage.io.actions;
				let actions = $actions.get(actionId, groupId);
				actions[idx] = action;
				$actions.set(actions, actionId, groupId);
			},
			del : function(idx){
				const $action = $data.storage.io.action;
				$action.del(idx)
			},
			get : function(idx, actionId, groupId) {
				$action = $data.storage.io.action;
				return $action.get(idx, actionId, groupId);
			},
			name : "$dataAction.item"
		},
		name : "$dataAction"
	}; // eof $data
	
	$(document).ready(function() {
		$dataAction.init();
	});
	
})();