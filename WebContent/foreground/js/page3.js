(function(){

	$page3 = {
		init : function() {
			$page3.ui.init();
			$page3.event.init();
		},
		ui : {
			init : function() {
				$page3.viewGroup.ui.init();
				$page3.viewList.ui.init();
			},
			selectItem : function(uuid) {
				 let jsObject = $dataItem.getItemByUUID(uuid);
				 if(jsObject != undefined) {
					 $page1.msgSend.contents.checkEl(jsObject);
					 $(`div.sel pre.select`).removeClass("select");
					 $(`div.sel[data-key=${uuid}] pre`).addClass("select");
					 $(`div.sel[data-key=${uuid}] pre`).focus();
				 }
			},


			name : "ui"
		},
		event : {
			init : function() {
				$page3.viewGroup.event.init();
				$page3.viewList.event.init();
			},
			onRecordView : function(jsObject) {
				$page3.viewList.ui.redraw();
			},
			name : "event"
		},
		msgSend : {
			contents : {
				getValue : function(jsObject, callback) {
					$("#tab3 div[data-id=dispVal]").text("");
					if (jsObject.attrType == "text"
							|| (jsObject.attrName != undefined && jsObject.attrName != "")) {
						let jsData = {
							selector : $data.util.getFullPath(jsObject),
							jsObject : jsObject
						};

						$message.msg.contents.send({
							action : "getValue",
							data : jsData
						}, function(response) {
							$("#tab3 div[data-id=dispVal]").text(response.value);
							if (callback) {
								callback(response);
							}
						});
					}
				},

				name : "$page3.msgSend.contents"
			},
			name : "$page3.msgSend"
		},
		viewGroup : {
			ui : {
				init : function() {
					const conEl = $("#tab3 select#view");
					let addId = function(viewId) {
						conEl.append($(`<option data-key="${viewId}">${viewId}</option>`));
					}
					
					conEl.empty()
					$dataView.group.getIds().forEach(function(item, idx) {
						addId(item);
					});
					
					$page3.viewGroup.ui.setId($layout.group.data.json.viewId);
				},
				getId : function() {
					const conEl = $("#tab3 select#view");
					let viewId = conEl.find("option:selected").data("key");
					return viewId;
				},
				setId : function(newId) {
					const conEl = $("#tab3 select#view");
					
					if(newId == undefined) {
						conEl.find(`option:not([data-key]:eq(0))`).prop("selected", true);
					} else {
						
						if(conEl.find(`option[data-key=${newId}]`).length == 0) {
							alert(`항목 [${newId}]을 찾을 수 없습니다.`);
							newId = conEl.find(`option[data-key]:eq(0)`).data("key");
						} else {
							conEl.find(`option[data-key=${newId}]`).prop("selected", true);
						}
					}
					$layout.group.data.json.viewId = newId;
					$layout.group.data.save();
				},
				name : "$page3.viewGroup.ui"
			},
			event : {
				init : function() {
					$("#viewSave").off("click").on("click", function(){
						
						let newName = window.prompt("저장할 View명을 입력 하세요.", $("#view").val());
						let ids = $dataView.group.getIds();
						let newIdx = -1;
						for(let i=0; i<ids.length; i++) {
							if(ids[i] == newName) {
								newIdx = i;
								break;
							}
						}
						if((newIdx < 0) || confirm("기존 항목에 덮어 쓰시겠습니까?")) {
							let views = $views.get();
							$dataView.group.save(views, newName);
							
							$page3.viewGroup.ui.init();
							$page3.viewGroup.event.init();
							$page3.viewGroup.ui.setId(newName);
						}

					});
					
					$("#viewReset").off("click").on("click", function(){
						const $group = $data.storage.io.group;
						const conEl = $("#tab3 select#view");
						// 그룹을 변경할때 이벤트
						let id = conEl.find("option:selected").data("key");
						if(id == undefined) {
							$dataView.group.del(undefined);
						} else {
							if(confirm(`[${id}]액션을 불러 올까요?\n* 현재 작업중인 내용은 삭제 됩니다.`)) {
								$dataView.group.load(id);
							} else {
								$page3.viewGroup.ui.setId($layout.group.data.json.viewId);
							}
						}
						$page3.init();
					});
					$("#viewDel").off("click").on("click", function(){
						const $group = $data.storage.io.group;
						const conEl = $("#tab3 select#view");
						let id = conEl.find("option:selected").data("key");
						
						if(id != undefined && confirm(`[${id}] 항목을 삭제 하시겠습니까?`)) {
							// 현재 선택한 ID를 삭제 함.
							$dataView.group.del(id);
							// 첫항목으로 셋팅 함.
							let newId = $dataView.group.getIds()[0];
							if(newId == undefined) {
								// 최종 항목일 경우... 
								$dataView.group.del(undefined);
							} else {
								// 항목이 남았을 경우..
								$dataView.group.load(newId);
							}
							$page3.viewGroup.ui.setId(newId);
							$page3.init();
							alert("삭제 되었습니다.(첫 항목으로 초기화 됩니다.)");
						} 
					});
					$("#view").off("change").on("change", function(){
						// 그룹을 변경할때 이벤트
						let viewId = $(this).find("option:selected").data("key");
						if(viewId != undefined) {
							if(confirm(`[${viewId}] 액션을 불러 올까요?(현재 작업중인 내용은 삭제 됩니다.)`)) {
								$dataView.group.load(viewId);
								$page3.viewGroup.ui.setId(viewId);
								$page3.init();
							}
							
						}
					});
					$("#viewPrev").off("click").on("click", function(){
						// 해당 View의 항목을 조회하녀 JSON으로 만들어 봄...

					});
				},

				
				name : "$page3.viewGroup.event"
			},
			name : "$page3.viewGroup"
		},
		viewList : {
			ui : {
				init : function() {
					this.redraw();
				},
				displayInfo : function(jsObject) {
					$(`#tab3 input[data-id=uuid]`).val(jsObject.uuid);
					$(`#tab3 input[data-id=alias]`).val(jsObject.alias == undefined ? "" : jsObject.alias);
					$(`#tab3 input[data-id=selector-AbsPath]`).val($data.util.getFullPath(jsObject));
					$(`#tab3 input[data-id=selector-RelativePath]`).val(jsObject.relativePath);
					if (jsObject.attrType != undefined && jsObject.attrType != "") {
						$(`#tab3 input[name=tab3-attrType][value=${jsObject.attrType}]`).prop("checked", true);
					} else {
						$(`#tab3 input[name=tab3-attrType]:checked`).prop("checked", false);
					}
					$(`#tab3 input[data-id=attrName]`).val(jsObject.attrName == undefined ? "" : jsObject.attrName);
					// contents에 해당 Element의 값을 조회 함.
					$page3.msgSend.contents.getValue(jsObject);

				},
				redraw : function() {
					const conEl = $("#tab3 [data-id=list_views]");
					const TRM = "    ";
					let term = "";
					let views = $dataView.group.get();
					let aliases = $dataItem.getAliases();
					conEl.empty();
					let delIdxs = [];
					$(views).each(function(idx, item) {
						
						alias = aliases.filter(c=>{return c.key == item.uuid});
						if(alias.length == 0) {
							// item 목록에서 삭제 됨.
							delIdxs.push(idx);
						} else {
							conEl.append($(`<div class="sel" data-key="${alias[0].key}"><pre>${alias[0].alias.trim()}</pre></div>`));
						}
						
					});

					for(let i=delIdxs.length-1; i>=0; i--) {
						$dataView.item.del(delIdxs[i]);
					}
				},
				name : "$page3.viewList.ui"
			},
			event : {
				init : function() {
					let conEl = $("#tab3 [data-id=list_views]");
					let fnToRelativePath = function(uuid, absPath) {
						let jsObject = $dataItem.getItemByUUID(uuid);
						let jsParentObject = $dataItem.getItemByUUID(jsObject.p_uuid);
						let pAbsPath = $data.util.getFullPath(jsParentObject);
						return absPath.replace(pAbsPath + " > ", "");
					};
					let item_onDblClick = function(item) {
						// Click Event 항목 노출..
						let uuid = $(item).data("key");
						let action = item.data("action");
						
						$(`div.sel pre.select`).removeClass("select");
						$(`div.sel[data-key=${uuid}] pre`).addClass("select");
						$page1.ui.selectItem(uuid);
						$tab.selectTab(0);
						
						$page2.actionList.ui.displayInfo({});
					};
					let fnModifyInfo = function() {
						const $ui = $page3.ui;

						// 변경된 값을 list 항목에 저장 함.
						let uuid = $("#tab3 input[data-id=uuid]").val();

						let jsObject = $dataItem.getItemByUUID(uuid);
						
						jsObject.alias = $("#tab3 input[data-id=alias]").val();
						jsObject.relativePath = fnToRelativePath(uuid, $("#tab3 input[data-id=selector-AbsPath]").val());
						jsObject.attrType = $("#tab3 input[name=tab3-attrType]:checked").val();
						jsObject.attrName = $("#tab3 input[data-id=attrName]").val();
						
						$dataItem.setItem(uuid, jsObject);
						
						// Item의 정보를 갱신 함.
//						$ui.refreshItems();
						$tab.tabs.ui.init();
						
						// itemList의 항목을 선택하게 함.
						$page1.ui.selectItem(uuid);
						// Contents영역에서 해당 항목의 값을 읽어 옴.
						$page3.msgSend.contents.getValue(jsObject);
						
					};

					conEl.off("dblclick", "div.sel").on("dblclick", "div.sel", function(){
						item_onDblClick($(this));
					});

					conEl.off("click", "div.sel").on("click", "div.sel", function(){
						let uuid = $(this).data("key");
						let idx = $dataView.item.toIdx(uuid);
						 let jsObject = $dataItem.getItemByUUID(uuid);

						$page3.ui.selectItem(uuid);
						$page3.viewList.ui.displayInfo(jsObject);
					 });
					$("#tab3 .attInfo").off("change", "input").on("change", "input", fnModifyInfo);
				},
				name : "$page3.viewList.event"
			},
			name : "$page3.viewList"
		},
		contextMenu : {
			deleteItem : function(orgUIEl) {
				let idx = $(orgUIEl).parent().index();
				$dataView.item.del(idx);
				$page3.viewList.ui.displayInfo({});
				$page3.viewList.ui.redraw();
			},
			name : "$page3.contextMenu"
			
		},
		name : "$page3"
	}; // eof $page3

// 	layout에서 UI 구성 후 호출 함.
//	$(document).ready(function(){
//		$page3.init();
//	});
})();
