(function() {
	$dataView = {
		init : function() {
		},
		group : {
			getIds : function(groupId) {
				$views = $data.storage.io.views;
				return $views.getIds(groupId);
			},
			load : function(viewId) {
				const $views = $data.storage.io.views;
				let views = $views.get(viewId);
				$views.set(views);
			},
			save : function(views, viewId) {
				const $views = $data.storage.io.views;
				$views.set(views, viewId);
			},
			get : function(viewId, groupId) {
				$views = $data.storage.io.views;
				return $views.get(viewId, groupId);
			},
			del : function(id){
				const $views = $data.storage.io.views;
				$views.del(id)
			},
			name : "$dataView.group"
		},
		item : {
			toIdx : function(uuid, viewId, groupId) {
				const $views = $data.storage.io.views;
				let views = $views.get(viewId, groupId);
				let idx = -1;
				for(let i=0; i<views.length; i++) {
					if(views[i].uuid == uuid) {
						idx = i;
						break;
					}
				}
				return idx;
			},
			push : function(view, viewId, groupId) {
				const $view = $data.storage.io.view;
				$view.add(view, viewId, groupId); 
			},
			add : function(idx, view, viewId, groupId) {
				const $views = $data.storage.io.views;
				let views = $views.get(viewId, groupId);
				
				let newViews = views.splice(0,idx)
								.concat(view)
								.concat(views);
				$views.set(newViews, viewId, groupId);
			},
			set : function(idx, view, viewId, groupId) {
				const $views = $data.storage.io.views;
				let views = $views.get(viewId, groupId);
				views[idx] = view;
				$views.set(views, viewId, groupId);
			},
			del : function(idx){
				const $view = $data.storage.io.view;
				$view.del(idx)
			},
			get : function(idx, viewId, groupId) {
				$view = $data.storage.io.view;
				return $view.get(idx, viewId, groupId);
			},
			name : "$dataView.item"
		},
		name : "$dataView"
	}; // eof $data
	
	$(document).ready(function() {
		$dataView.init();
	});
	
})();