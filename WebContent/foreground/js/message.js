(function() {

	$message = {
		port : undefined,
		init : function() {
			$message.event.init();
		},
		event : {
			init : function() {
				// Content Window로 부터 Message 수신을 위한 Handler 등록
				chrome.runtime.onMessage.addListener($message.msg.contents.receive);
				// Background로 부터 Message 수신을 위한 Handler 등록
				window.addEventListener("message", ($message.msg.extension.receive));
				
				$message.port = chrome.runtime.connect({name : "ns-template-builder-browser"});
			},
			name : "event"
		},
		alert: {
			show : function(msg) {
				window.setTimeout(function(){
					window.focus();
					alert(msg);
				}, 1);
			}
		},

		msg : {
			contents : {
				send : function(json, callback) {
					chrome.tabs.query({
						active : true,
						currentWindow : false
					}, function(tabs) {
						try{
							chrome.tabs.sendMessage(tabs[0].id, json,
									function(response) {
										if (response != undefined) {
											if(callback) {
												callback(response);
											}
										} else {
											$message.alert.show("크롬 브라우져를 새로고침 해 주세요!");
										}
									});
						}
						catch(e) {
							$message.alert.show(e);
						}
					})
				},
				receive : function(request, sender, sendResponse) {
					console.log("$message.msg.contents.receive() ::" + request.action);
					const fnAddNewItems = function(request) {
						let newUUID = $dataItem.setContentsItems(request.selector, undefined);
						$tab.tabs.event.onContentsSelect(request.selector, request.stat, request.text);

						return newUUID;
					} ;
					const fnAddNewView = function(jsObject, actionId) {
						$dataView.item.push(jsObject, actionId, undefined);
						$tab.tabs.event.onRecordView(jsObject);
					};
					const fnAddNewAction = function(jsObject, actionId) {
						$dataAction.item.push(jsObject, actionId, undefined);
						$tab.tabs.event.onRecordAction(jsObject);
					}
					
					if (request.action) {
						switch (request.action) {
						case 'selectItem':
							{
								// contents 영역에서 element를 선택 한 이벤트를 수신 함.
								let newUUID = fnAddNewItems(request);
								fnAddNewView({action : request.command, uuid : newUUID});
							}
							break;
						case 'recordAction':
							{
								// Click Event를 기록 함.
								// 선택한 항목을 Node에 추가 함.
								let newUUID = fnAddNewItems(request);
								fnAddNewAction({action : request.command, uuid : newUUID});
							}
							break;
						}

					} else {
						sendResponse({
							"result" : "FAIL"
						});
					}
				},
			},
			extension : {
				send : function(json, callback) {
					if(callback != undefined) {
						callback(json);
					}
				},
				receive : function(e) {
					/**
					 * Background 로 부터 Window PostMessage를 받음.
					 */
					// Contents 영역으로 전달 함.
					console.log("receive post msg : " + e.data);
					$message.msg.contents.send({ action : e.data });
				},

				name : "$message.msg.extension"
			},

			name : "msg"
		},
		name : "$message"

	}; // eof page1

	$(document).ready(function() {
		$message.init();
	});
})();
