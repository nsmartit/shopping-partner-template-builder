(function() {

	$net = {
		post : {
			send : function(url, jsData, fnCallback) {
				$.ajax({
					type : "POST",
					url : url,
					data : JSON.stringify(jsData),
					async: false,
					contentType : "application/json",
					success : function(data) {
						if (fnCallback != undefined) {
							fnCallback(data);
						}
					},
					error : function(data) {
						alert("통신 에러 발생!!");
					}
				});
			},
			name : "$net.post"
		},
		get : {
			send : function(url) {
				
			},
		},
		name : "$net"
	};

	// $(document).ready(function() {
	// $net.init();
	// });

})();