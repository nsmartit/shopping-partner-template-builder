(function() {

	$page1 = {
		init : function() {
			console.log("init $page1");
			// page1
			$page1.ui.init();
			$page1.event.init();
		},
		// vue : {
		// item : new Vue({
		// el: '.attInfo',
		// data: {
		// input: '. attInfo'
		// },
		// computed: {
		// compiledMarkdown: function () {
		// return marked(this.input, { sanitize: true })
		// }
		// },
		// methods: {
		// update: _.debounce(function (e) {
		// this.input = e.target.value
		// }, 300)
		// }
		// }),
		// name : "vue"
		// },
		msgSend : {
			contents : {
				getValue : function(jsObject, callback) {
					$("#tab1 div[data-id=dispVal]").text("");
					if (jsObject.attrType == "text"
							|| (jsObject.attrName != undefined && jsObject.attrName != "")) {
						let jsData = {
							selector : $data.util.getFullPath(jsObject),
							jsObject : jsObject
						};

						$message.msg.contents.send({
							action : "getValue",
							data : jsData
						}, function(response) {
							$("#tab1 div[data-id=dispVal]").text(response.value);
							if (callback) {
								callback(response);
							}
						});
					}
				},
				checkEl : function(jsObject) {
					let jsData = {
						selector : $data.util.getFullPath(jsObject)
					};

					// element의 속성을 표시 함.
					$page1.ui.displayInfo(jsObject);

					// Contents window에 전달 함.
					$message.msg.contents.send({
						action : "checkEl",
						data : jsData
					}, function(response) {
						if (response.result == "FAIL") {
							alert("선택한 항목을 Contents 영역에서 찾을 수 없습니다.");
						}
					});
				},
				name : "$page1.msgSend.contents"
			},
			name : "$page1.msgSend"
		},
		ui : {
			init : function() {
				$page1.ui.refreshItems();
			},
			itemRedraw : function() {
				let conEl = $("#tab1 [data-id=list_items]");
				conEl.empty();
				$($dataItem.getAliases())
					.each(function() {
							let mustStr = (this.item.must=="Y" ? " must" : "");
							conEl.append($(`<div class="sel${mustStr}" data-key="${this.key}"><pre>${this.alias}</pre></div>`));
						});
			},
			refreshItems : function() {
				// $page1.ui.displayInfo({});
				$page1.ui.itemRedraw();
				$page1.event.itemsEventBind();
			},
			displayInfo : function(jsObject) {
				$(`#tab1 input[data-id=uuid]`).val(jsObject.uuid);
				$(`#tab1 input[data-id=alias]`).val(jsObject.alias == undefined ? "" : jsObject.alias);
				$(`#tab1 input[data-id=selector-AbsPath]`).val($data.util.getFullPath(jsObject));
				$(`#tab1 input[data-id=selector-RelativePath]`).val(jsObject.relativePath);
				if (jsObject.attrType != undefined && jsObject.attrType != "") {
					$(`#tab1 input[name=tab1-attrType][value=${jsObject.attrType}]`).prop("checked", true);
				} else {
					$(`#tab1 input[name=tab1-attrType]:checked`).prop("checked", false);
				}
				$(`#tab1 input[data-id=attrName]`).val(jsObject.attrName == undefined ? "" : jsObject.attrName);
				if (jsObject.must == undefined || jsObject.must == "") {
					jsObject.must = "N";
				}
				$(`#tab1 input[name=tab1-must][value=${jsObject.must}]`).prop("checked", true);

				// contents에 해당 Element의 값을 조회 함.
				$page1.msgSend.contents.getValue(jsObject);

			},
			selectItem : function(uuid, groupId) {
				 let jsObject = $dataItem.getItemByUUID(uuid, groupId);
				 if(jsObject != undefined) {
					 $page1.msgSend.contents.checkEl(jsObject);
					 $(`div.sel pre.select`).removeClass("select");
					 $(`div.sel[data-key=${uuid}] pre`).addClass("select");
					 $(`div.sel[data-key=${uuid}] pre`).focus();
				 }
			},
			name : "$page1.ui"
		},
		event : {
			init : function() {
				$page1.event.itemsEventBind();
				$page1.event.attrChangeEventBind();
			},
			itemsEventBind : function(groupId) {
				const $ui = $page1.ui;
				let conEl = $("#tab1 [data-id=list_items]");
				conEl.off("click", "div.sel").on("click", "div.sel", function(){
//					 let jsObject = $dataItem.getItemByUUID($(this).data("key"), groupId);
//					 $page1.msgSend.contents.checkEl(jsObject);
					$page1.ui.selectItem($(this).data("key"), groupId);
				 });
				
			},
			attrChangeEventBind : function(groupId) {
				let fnToRelativePath = function(uuid, absPath) {
					let jsObject = $dataItem.getItemByUUID(uuid, groupId);
					let jsParentObject = $dataItem.getItemByUUID(jsObject.p_uuid, groupId);
					let pAbsPath = $data.util.getFullPath(jsParentObject);
					return absPath.replace(pAbsPath + " > ", "");
				};
				
				let fnModifyInfo = function() {
					const $ui = $page1.ui;

					// 변경된 값을 list 항목에 저장 함.
					let uuid = $("#tab1 input[data-id=uuid]").val();

					let jsObject = $dataItem.getItemByUUID(uuid, groupId);
					jsObject.alias = $("#tab1 input[data-id=alias]").val();
					jsObject.relativePath = fnToRelativePath(uuid, $("#tab1 input[data-id=selector-AbsPath]").val());
					jsObject.attrType = $("#tab1 input[name=tab1-attrType]:checked").val();
					jsObject.attrName = $("#tab1 input[data-id=attrName]").val();
					jsObject.must = $("#tab1  input[name=tab1-must]:checked").val();
					$dataItem.setItem(uuid, jsObject, groupId);
					
					// Item의 정보를 갱신 함.
//					$ui.refreshItems();
					$tab.tabs.ui.init();
					
					// itemList의 항목을 선택하게 함.
					$page1.ui.selectItem(uuid, groupId);

					// Contents영역에서 해당 항목의 값을 읽어 옴.
					$page1.msgSend.contents.getValue(jsObject);
				};

				$("#tab1 .attInfo").off("change", "input").on("change", "input", fnModifyInfo);
			},
			onContentsSelect : function(cssSelectorArr, stat, strElTxt) {
				/**
				 * Contents Element를 선택 함.
				 * 
				 * @cssSelectorArr Contents 영역에서 선택한 Element의 CSS Selector 배열
				 */
				$page1.ui.displayInfo({});
				$page1.ui.refreshItems();
			},

			name : "$page1.event"
		},
		contextMenu : {
			deleteItem : function(orgUIEl) {
				if($(`#tab1 input[data-id=uuid]`).val() == "") {
					alert("삭제할 항목을 선택 하세요.");
				} else {
					let alias = $("#tab1 input[data-id=alias]").val() == "" ? $("#tab1 input[data-id=uuid]").val() : $("tab1 input[data-id=alias]").val();
					if(confirm(`[${alias}] 항목을 삭제 하시겠습니까?`)) {
						$dataItem.deleteItem($("#tab1 input[data-id=uuid]").val())
						$page1.ui.refreshItems();
						$page1.ui.displayInfo({});
					}
				}
			},			
			name : "$page1.contextMenu"
			
		},

		name : "$page1"
	}; // eof $page1

	// layout에서 UI 구성 후 호출 함.
	// $(document).ready(function() {
	// setTimeout(, 1);
	// });
})();
