(function() {

	$dataItem = {
		init : function() {
			// Repository를 비움...
			// $$buff.clear();
			
			// 서버에서 받아 옴..
			
		},

		// ---------------------------------------------------------------------
		getItemByUUID : function(uuid, groupId) {
			const item = $data.storage.io.item;
			return item.getByUUID(uuid, groupId);
		},


		getItemByCss : function(strSelector, groupId) {
			const item = $data.storage.io.item;
			return item.getByCss(strSelector, groupId);
		},
		setContentsItems : function(selectInfoArr, groupId) {
			const $util = $data.util;
			const $items = $data.storage.io.items;
			
			// ### UUID를 생성 함
			const fn_getItemByUUID = $dataItem.getItemByUUID;
			let fn_getUUID = function() {
				let uuid = "";
				do {
					uuid = $data.util.getUUID();
				} while (fn_getItemByUUID(uuid) != undefined);
				return uuid;
			};

			// ### FullPath를 사용하여 기존에 저장된 Node와 중복을 제거 하고, 신규로 추가된 항목들을 담는다.
			let fn_addNewElements = function(selectInfoArr, groupId) {
				let items = $items.get(groupId);
				let newUUID = undefined;
				for (let i = selectInfoArr.length - 1; i >= 0; i--) {
					// 현재 Object를 $$buff에서 동일한 element가 있는지 검색 한다.
					let object = $dataItem.getItemByCss(selectInfoArr[i], groupId);
					let uuid = "";
					
					if (object != undefined) {
						uuid = object.uuid;
					} else {
						// 기존에 등록된 항목이 없는 경우...
						uuid = fn_getUUID();
						// absPath항목은 이제 더이상 사용하지 않는다.
						// (만약 성능 이슈가 발생 한다면, 사전에 모든 항목에 대해 update 후에 작업 끝난 후 삭제
						// 하도록 한다.)
						delete selectInfoArr[i].absPath;
						selectInfoArr[i].uuid = uuid;
						items[uuid] = selectInfoArr[i];
					}

					// selectInfoArr 다음 항목의 p_uuid를 현재 항목으로 지정 한다.
					// (배열에 root->node의 순서로 저장되어 있음.)
					if (i > 0) {
						selectInfoArr[i - 1].p_uuid = uuid;
					}
					
					newUUID = uuid;
				}
				$items.set(items, groupId);
				return newUUID;
			};
			
			return fn_addNewElements(selectInfoArr, groupId);
		},

		getAlias : function(jsObject) {
			let alias = jsObject.alias;
			let uuid = jsObject.uuid;
			return (alias == undefined || alias == "" ? uuid : alias);
		},
		getAliases : function(groupId) {
			const DUMMY = "  ";
			let dispItems = [];
			let trees = $dataItem.getTree(groupId);

			let fn_getDispItem = function(node, dummyStr) {
				return {
					parent : node.parent,
					alias : `${dummyStr}${node.alias}`,
					key : node.item.uuid,
					item : node.item,
					childs : node.childs
				};
			};
			let fn_getNode2DispItem = function(dispItem, dstr, node) {
				dispItems.push(fn_getDispItem(node, dstr));
				for(let i=0; i<node.childs.length; i++) {
					fn_getNode2DispItem(dispItem, `${dstr}${DUMMY}`, node.childs[i]);
				}
			};
			
			trees.forEach(function(tree, idx){
				fn_getNode2DispItem(dispItems, "", tree);
			});
			return dispItems;
		},

		getTree : function(groupId) {
			const $items = $data.storage.io.items;
			const $item = $data.storage.io.item;
			
			let items = $items.get(groupId);
			let keys = Array.from(Object.keys(items));

			let fn_getAlias = $dataItem.getAlias;
			
			let fn_findRootNode = function(items, keys) {
				let rootEl = [];
				for(let i=0; i<keys.length; i++) {
					let o = items[keys[i]];
					if(o.p_uuid == undefined || o.p_uuid == "") {
						// root Element가 여러개인 경우가 있음..(Layer 같은 경우...)
//						rootEl = o;
//						break;
						rootEl.push(o);
					}
				}
				return rootEl;
			};
			
			let fn_getTree = function(items, keys, root) {
				if(root == undefined) 
					return undefined;
				let childKeys = keys.filter(s => {return items[s].p_uuid == root.uuid;});
				let childNodes = [];
				for(let i=0; i<childKeys.length; i++) {
					let childNode = fn_getTree(items, keys, items[childKeys[i]]);
					childNodes.push(childNode);
				}
				// tree에 포함되지 않는 항목 debuging용...
				root["tree"] = "1";
				return {
							parent : items[root.p_uuid],
							item : root,
							childs : childNodes,
							alias : fn_getAlias(root)
						};
			};
			for(let i=0; i<items.length; i++) {
				delete items[i].tree;
			}
			let roots = fn_findRootNode(items, keys);
			let trees = [];
			roots.forEach(function(root, idx){
				trees.push(fn_getTree(items, keys, root));
			});
			
			return trees;
		},
		
		deleteItem : function(key){
			$item = $data.storage.io.item;
			$item.del(key);
		},
		setItem : function(uuid, jsObject, groupId) {
			const $item = $data.storage.io.item;
			$item.set(uuid, jsObject, groupId);
		},
		name : "$dataItem"
	}; // eof $data

	
	$(document).ready(function() {
		$dataItem.init();
	});
})();