(function(id) {
// "use strict";
	
	$contextMenu = {
		_opts : {
			classes : {
				ac_show 	: "show"
			,	ac_hide 	: "hide"
//			,	menu 		: "_0mzbd0_menu_"
//			,	menu_opts 	: `_0mzbd0_menu-options_`
//			,	menu_opt 	: `_0mzbd0_menu-option_`
			}
		},
		_target : undefined,
		init : function(id) {
			if(id == undefined) {
				id = "_0mzbd0_";
			}
			$contextMenu._opts.classes.id = id;
			$contextMenu._opts.classes.menu = `${id}-menu`;
			$contextMenu._opts.classes.menu_opts = `${id}-menu-options_`;
			$contextMenu._opts.classes.menu_opt = `${id}-menu-option_`;
			
			
			$contextMenu.ui.initUI();
			$contextMenu.event.bindEvent();
		},
		ui : {
			initUI : function() {
				const genMenu = ({items}) => {
					let tag = ``;
					tag = tag + (`<div class="${$contextMenu._opts.classes.menu}">`);
					tag = tag + (`  <ul class="${$contextMenu._opts.classes.menu_opts}">`);
					items.forEach(function(item){
						tag = tag + (`    <li class="${$contextMenu._opts.classes.menu_opt}" data-on="${item.on}" data-off="${item.off}" data-key="${item.key}">${item.off}</li>`);
					});
					tag = tag + (`  </ul>`);
					tag = tag + (`</div>`);
					
					$("body").append(tag);
				};
				genMenu({
						  items : [
									{key:"selectItem", on:"항목추출중지", off:"항목추출시작"}
								  ,	{key:"recordAction", on:"동작기록중지", off:"동작기록시작"}
								  ]
						});
			},
			resetMenuText : function() {
				/**
				 * ContextMenu의 Text를 상태에 맞게 노출 함.
				 **/
				$(`.${$contextMenu._opts.classes.menu} .${$contextMenu._opts.classes.menu_opt}`).each((index, item) => {
					let key = $(item).data("key");
					let state = $contents.stat[key] ? "on" : "off";
					let text = $(item).data(state);
					$(item).text(text);
				});
			},
			getMenuObject : function() {
				return document.querySelector(`.${$contextMenu._opts.classes.menu}`);
			},
			toggleMenu : function(command) {
				const menu = this.getMenuObject();
				let menuVisible = (command === $contextMenu._opts.classes.ac_show);
				menu.style.display = menuVisible ? "block" : "none";
			},
			isShow : function() {
				const menu = this.getMenuObject();
				return menu.style.display == "block";
			},
			show : function(left, top, targetEl) {
				const menu = this.getMenuObject();
				if(!this.isShow()) {
					// Refresh Menu Item Text
					this.resetMenuText();
					// Set TargetEl
					$contextMenu._target = targetEl;
					// Show Menu
					menu.style.left = `${left}px`;
					menu.style.top = `${top}px`;
					this.toggleMenu($contextMenu._opts.classes.ac_show);
				}
			},
			hide : function() {
				if(this.isShow()) {
					this.toggleMenu($contextMenu._opts.classes.ac_hide);
					$contextMenu._target = undefined;
				}
			},
			name : "ui"
		}, 
		event : {
			bindEvent : function() {
				const onHideMenu = function(e) {
					let isShow = $contextMenu.ui.isShow(); 
					if(isShow) {
						e.preventDefault();
						$contextMenu.ui.hide();
					}
					return !isShow;
				};
				const onClickMenuItem = function(e) {
					let isShow = $contextMenu.ui.isShow(); 
					if(isShow) {
						e.preventDefault();
						$contextMenu.ui.hide();

						$contextMenu.event.menuSelect(e.target, $contextMenu._target);
					}
					
					return !isShow;
				};

				// DEFAULT EVENT LISTENER //
				window.addEventListener("click", onHideMenu);
				// JQUERY EVENT LISTENER :: ui 이벤트에서 이벤트 전파를 취소 하면 window의 이벤트는 동작하지 않는다. //
				$(`[class^=${$contextMenu._opts.classes.id}]`)
					.off()
					.on("click", onClickMenuItem)
					;

				window.addEventListener("contextmenu", e => {
					if($contents.stat.context) {
						e.preventDefault();
						$contextMenu.ui.show(e.pageX, e.pageY, e.srcElement);
					}

					return false;
				});
			},
			menuSelect : function(menuItem, orgUIEl) {
				/**
				 * 팝업 메뉴를 선택 함.
				 * 
				 * @param menuItem
				 *            선택한 팝업메뉴의 항목
				 * @param orgUIEl
				 *            팝업을 열때 선택한 화면의 Element
				 */
				let retval = false;
				// context menu 선택 함.
				let jqMenuItem = $(menuItem);

				// 여기에 옵션에 따른 처리를 하도록 기능 추가 함.
				let key = jqMenuItem.data("key");
				let $action = (key == undefined ? undefined : $contents.action[key]);
				let stat = (key == undefined ? undefined : $contents.stat[key]);
				console.log(`key=${key}, action=${$action}, stat=${stat}`);
				
				if($action != undefined && stat != undefined) {
					if(stat) {
						$action.off();
					} else {
						// 현재 켜져 있는 다른 옵션을 모두 끔.
						Array.from(Object.keys($contents.stat)).forEach(function(){
							if($contents.stat[this]) {
								$contents.action[this].off();
							}
						});

						$action.on();
					}
					retval = true;
				}
				return retval;
			},
			name : "event"
		}
		
	};

	$(document).ready(function(){
		$contextMenu.init();
	});
})();