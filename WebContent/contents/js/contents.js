(function() {
	$contents = {
		init : function(id) {
			console.log("$contents.init!!");
			$contents.event.readyMsg();
			// $contents.event._binding();
		},
		stat : {
			context : false,
			selectItem : false,
			recordAction : false
		},
		ui : {
			effect : {
				itemSelectShow : function(el) {
					
					let targetEl = $(el);
					
					let elInfo = {
						"left" : targetEl.offset().left,
						"top" : targetEl.offset().top,
						"width" : targetEl.width(),
						"height" : targetEl.height(),
						"opacity" : "0",
						"background-color" : targetEl.css("background-color"),
						"border-color" : targetEl.css("border-color")
					};
					
					let fnGetEl = function() {
						let displayBox = $("div#ns_display_box");
						if (displayBox.length == 0) {
							displayBox = $(`<div id="ns_display_box" style="z-index:9999999999;"></div>`);
							$("body").append(displayBox);
						}
						return displayBox;
					};

					let fnAnimation = function(dispBox, tw, th, delay) {
						let aniBaseInfo = {
							"position" : "absolute",
							"top" : elInfo.top - tw,
							"left" : elInfo.left - th,
							"width" : elInfo.width + (tw << 1),
							"height" : elInfo.height + (th << 1),
							"background-color" : "#030303",
							"border-width" : 2,
							"border-color" : "#FF0000",
							"display" : "block",
							"opacity" : "1"
						};
						// #div 좌표 새로 설정
						
						// 이미 실행중이던 Animation이 있으면 중지 시킴
						dispBox.clearQueue();
						dispBox.stop();
						
						// 새로운 Animation을 시작 시킴
						dispBox
							.show()
							.css(aniBaseInfo)
//							.animate(elInfo, delay >> 2)
//							.animate(aniBaseInfo, delay >> 2)
							.animate(elInfo, delay >> 1).promise().then(
										function() {
											dispBox.hide();
										});
					};

					fnAnimation(fnGetEl(), 4, 4, 500);
				},
				name : "$contents.ui.effect"
			},
			name : "$contents.ui"
		},
		event : {
			_opts : {
				defName : "_0mzbd0_",
				clsFocusEventItem : "_0mzbd0-focus_",
				clsArrayItems : "_0mzbd0-item_",
				clsArrayContainer : "_0mzbd0-container_"
			},

			readyMsg : function() {
				/**
				 * Browser로 부터 수신될 메시지 Listener 설정
				 */
				let waitFromBrowser = function() {
					console.log("contents ready receive message from window");
					chrome.runtime.onMessage
							.addListener($contents.msg.browser.receive);
				};
				
				let waitFromExtension = function() {
					$contents.msg.extension.port = chrome.runtime.connect({
						name : "ns-template-builder-contents"
					});
					$contents.msg.extension.port.onMessage.addListener(function(msg) {
						$contents.msg.extension.receive(msg);
					});
				};
				
				// Browser로 부터 수신 대기
				waitFromBrowser();
				// 확장프로그램으로 부터 수신 대기
				waitFromExtension();
				
			},
			name : "$contents.event"
		},
		msg : {
			cons : {
				success : "SUCCESS",
				fail : "FAIL"
			},
			browser : {
				send : function(json, callback) {
					chrome.runtime.sendMessage(json, function(response) {
						if (response != undefined) {
							console.log(response.result);
							if (callback != undefined) {
								callback(response);
							}
						}
					});
					name: "$contents.msg.browser.send"
				},
				receive : function(request, sender, sendResponse) {
					console.log("request.action=" + request.action);
					let responseResult = undefined;
					try {
						switch (request.action) {
						case "on":
							$contents.action.contextMenu.on();
							responseResult = $contents.msg.cons.success;
							break;
						case "off":
							$contents.action.contextMenu.off();
							responseResult = $contents.msg.cons.success;
							break;
						case "enableSelector":
							$contents.action.selectItem.on();
							responseResult = $contents.msg.cons.success;
							break;
						case "desableSelector":
							$contents.action.selectItem.off();
							responseResult = $contents.msg.cons.success;
							break;
						case "checkEl":
							responseResult = $contents.action.effect.selectItem(request.data);
							break;
						case "getValue":
							sendResponse({
								result : $contents.msg.cons.success,
								value : $contents.action.value.get(request.data)
							});
							break;
						case "actionPrev":
							sendResponse({
								result : $contents.msg.cons.success,
								value : $contents.action.value.action(request.data)
							});
							break;
						default:
							responseResult = $contents.msg.cons.fail;
							break;
						}
					} catch (e) {
						responseResult = $contents.msg.cons.fail;
					}
					if (responseResult != undefined) {
						sendResponse({
							"result" : responseResult
						});
					}
				},

				name : "$contents.msg.browser"
			},
			extension : {
				port : undefined,
				send : function(json, callback) {
					this.port.postMessage(json, function(response) {
						if (response != undefined) {
							console.log(response.result);
							if (callback != undefined) {
								callback(response);
							}
						}
					});

				},
				receive : function(data) {
					try {
						switch (data.action) {
						case "on":
							$contents.action.contextMenu.on();
							break;
						case "off":
							$contents.action.contextMenu.off();
							break;
						}
					} catch (e) {
					}
				},
				name : "$contents.msg.extension"
			},
			name : "$contents.msg"
		},
		action : {
			contextMenu : {
				on : function() {
					$contents.stat.context = true;
					$contextMenu.ui.hide();
					return $contents.msg.cons.success;
				},
				off : function() {
					$contents.stat.context = false;
					$contextMenu.ui.hide();
					return $contents.msg.cons.success;
				},
				name : "$contents.action.contextMenu"
			},

			selectItem : {
				on : function() {
					this.event.bind();
					$contents.stat.selectItem = true;
					return $contents.msg.cons.success;
				},
				off : function() {
					this.event.unbind();
					$contents.stat.selectItem = false;
					return $contents.msg.cons.success;
				},
				event : {
					bind : function() {
						console.log("bindUIEvent call!!");
						const $util = $contents.util;

						// mouse click event
						const fnClick = function(e) {
							e.preventDefault();
							let selector = $util.getSelectorPathOfCss(undefined, $(this));
							let text = $(selector).text().trim().substring(0, 100);

							// Context Menu 닫기
							$contextMenu.ui.hide();
							
							$contents.msg.browser.send({
								action : "selectItem",
								stat : $contents.stat,
								selector : selector,
								text : text
							});
							
							$contents.action.selectItem.event.bind();
							
							return false;
						};
						// mouse enter event
						const fnMouseenter = function() {
							// console.log("mouse over");
							const clsName = $contents.event._opts.clsFocusEventItem;
							$(this).attr(clsName, clsName);
							$(this).parents().removeAttr(clsName);
						}
						// mouse leave event
						const fnMouseleave = function() {
							const clsName = $contents.event._opts.clsFocusEventItem;
							if ($(this).tagName !== "body") {
								$(this).removeAttr(clsName);
							}
						};

						$(`*:not([class^=${$contextMenu._opts.classes.id}])`).attr($contents.event._opts.defName, "_");
						let selectorStr = `[${$contents.event._opts.defName}]`;
						[
							{ event:"click", callback:fnClick },
							{ event:"mouseenter", callback:fnMouseenter},
							{ event:"mouseleave", callback:fnMouseleave}
						].forEach(function(item){
							$("*")
							.off(item.event, selectorStr)
							.on( item.event, selectorStr, item.callback);
						});
//						$("*")
//							.off("click",     `[${$contents.event._opts.defName}]`).on("click",      `[${$contents.event._opts.defName}]`, fnClick)
//							.off("mouseenter, `[${$contents.event._opts.defName}]`").on("mouseenter",`[${$contents.event._opts.defName}]`, fnMouseenter)
//							.off("mouseleave",`[${$contents.event._opts.defName}]`).on("mouseleave", `[${$contents.event._opts.defName}]`, fnMouseleave);

						// end bindUIEvent //
					},
					unbind : function() {
						console.log("unbindUIEvent call!!");
						/**
						 * 메시지 전송 포트 생성. 이벤트핸들러 설정
						 */
						[ { sel : `[${$contents.event._opts.defName}]`          , attr : $contents.event._opts.defName }
						, { sel : `[${$contents.event._opts.clsFocusEventItem}]`, attr : $contents.event._opts.clsFocusEventItem }
						, { sel : `[${$contents.event._opts.clsArrayItems}]`    , attr : $contents.event._opts.clsArrayItems }
						, { sel : `[${$contents.event._opts.clsArrayContainer}]`, attr : $contents.event._opts.clsArrayContainer } ]
						.forEach(function(item) {
							$("*")
							.off("click",item.sel)
							.off("mouseenter",item.sel)
							.off("mouseleave",item.sel);
							$(item.sel).removeAttr(item.attr);
						});

//						$(`[${$contents.event._opts.defName}]`).off().removeAttr($contents.event._opts.defName);
//						$(`[${$contents.event._opts.clsFocusEventItem}]`).off().removeAttr($contents.event._opts.clsFocusEventItem);
//						$(`[${$contents.event._opts.clsArrayItems}]`).off().removeAttr( $contents.event._opts.clsArrayItems);
//						$(`[${$contents.event._opts.clsArrayContainer}]`).off().removeAttr($contents.event._opts.clsArrayContainer);
					},
				},
				name : "$contents.action.selectItem"
			},
			
			recordAction : {
				on : function() {
					this.event.bind();
					$contents.stat.recordAction = true;		
				},
				off : function() {
					this.event.unbind();
					$contents.stat.recordAction = false;
				},
				event : {
					bind : function() {
						console.log("bindUIEvent call!!");
						const $util = $contents.util;

						// mouse click event
						const fnClick = function(e) {
							e.preventDefault();

							let selector = $util.getSelectorPathOfCss(undefined, $(this));
//							let text = $(selector).text().trim().substring(0, 100);

							// Context Menu 닫기
							$contextMenu.ui.hide();
							
							$contents.msg.browser.send({
								action : "recordAction",
								command : "click",
								selector : selector
							});

							return false;
						};
						// mouse enter event
						const fnMouseenter = function() {
							// console.log("mouse over");
							const clsName = $contents.event._opts.clsFocusEventItem;
							$(this).attr(clsName, clsName);
							$(this).parents().removeAttr(clsName);
						}
						// mouse leave event
						const fnMouseleave = function() {
							const clsName = $contents.event._opts.clsFocusEventItem;
							if ($(this).tagName !== "body") {
								$(this).removeAttr(clsName);
							}
						};

						$(`*:not([class^=${$contextMenu._opts.classes.id}])`).attr($contents.event._opts.defName, "_");
						let selectorStr = `[${$contents.event._opts.defName}]`;
						[
							{ event:"click", callback:fnClick },
							{ event:"mouseenter", callback:fnMouseenter},
							{ event:"mouseleave", callback:fnMouseleave}
						].forEach(function(item){
							$("*")
							.off(item.event, selectorStr)
							.on( item.event, selectorStr, item.callback);
						});
//						$("*")
//							.off("click",     `[${$contents.event._opts.defName}]`).on("click",      `[${$contents.event._opts.defName}]`, fnClick)
//							.off("mouseenter, `[${$contents.event._opts.defName}]`").on("mouseenter",`[${$contents.event._opts.defName}]`, fnMouseenter)
//							.off("mouseleave",`[${$contents.event._opts.defName}]`).on("mouseleave", `[${$contents.event._opts.defName}]`, fnMouseleave);

						// end bindUIEvent //
					},
					unbind : function() {
						console.log("unbindUIEvent call!!");
						/**
						 * 메시지 전송 포트 생성. 이벤트핸들러 설정
						 */
						[ { sel : `[${$contents.event._opts.defName}]`          , attr : $contents.event._opts.defName }
						, { sel : `[${$contents.event._opts.clsFocusEventItem}]`, attr : $contents.event._opts.clsFocusEventItem }
						, { sel : `[${$contents.event._opts.clsArrayItems}]`    , attr : $contents.event._opts.clsArrayItems }
						, { sel : `[${$contents.event._opts.clsArrayContainer}]`, attr : $contents.event._opts.clsArrayContainer } ]
						.forEach(function(item) {
							$("*")
							.off("click",item.sel)
							.off("mouseenter",item.sel)
							.off("mouseleave",item.sel);
							$(item.sel).removeAttr(item.attr);
						});

//						$(`[${$contents.event._opts.defName}]`).off().removeAttr($contents.event._opts.defName);
//						$(`[${$contents.event._opts.clsFocusEventItem}]`).off().removeAttr($contents.event._opts.clsFocusEventItem);
//						$(`[${$contents.event._opts.clsArrayItems}]`).off().removeAttr( $contents.event._opts.clsArrayItems);
//						$(`[${$contents.event._opts.clsArrayContainer}]`).off().removeAttr($contents.event._opts.clsArrayContainer);
					},
				},
				name : "$contents.action.recordAction"
			},
			
			effect : {
				selectItem : function(jsParamData) {
					let targetEl = $(jsParamData.selector);
					let result = undefined;
					if (targetEl.length > 0) {
						// SelectEffect를 연출 함
						$contents.ui.effect.itemSelectShow(targetEl);
						result = $contents.msg.cons.success;
					} else {
						result = $contents.msg.cons.fail;
					}
					return result;
				},
				name : "$contents.action.effect"
			},

			value : {
				get : function(data) {
					let targetEl = $(data.selector);
					let value = undefined;

					if (targetEl.length > 0) {
						switch (data.jsObject.attrType) {
						case "text":
							value = targetEl.text();
							break;
						case "attr":
							value = targetEl.attr(data.jsObject.attrName);
							break;
						case "prop":
							value = targetEl.attr(data.jsObject.attrName);
							break;
						case "data":
							value = targetEl.data(data.jsObject.attrName);
							break;
						}
						// SelectEffect를 연출 함
						$contents.ui.effect.itemSelectShow(targetEl);

					} else {
						// alert("not find!!");
						throw Exception("항목을 찾을 수 없습니다.");
					}
					return value;
				},
				action : function(data) {
					$actionPlayer.init(data);
					return $actionPlayer.play();
				},
				name : "$contents.action.value"
			},
			name : "$contents.action"
		},
		util : {
			getSelectorPathOfCss : function(containerEl, el) {

				let _getNodeIdx = function(el) {
					/**
					 * 해당 Dom의 인덱스를 구한다. name이 없다면 nodeName만 같은 항목 중에서 인덱스를 구한다.
					 * 만약 name이 있다면, nodeName과 name 모두 같은 항목에서 인덱스를 구한다.
					 * 
					 * @param el :
					 *            인덱스를 구하고자 하는 element
					 */

					let htmlEl = $(el)[0];
					let siblings = $(el).parent().children();
					let idx = 0;
					let elInfo = {
						nodeName : htmlEl.nodeName.toLowerCase(),
						name : htmlEl.name
					};
					for (let i = 0; siblings.length; i++) {
						if (siblings[i] == htmlEl) {
							break;
						} else if (siblings[i].nodeName.toLowerCase() == elInfo.nodeName
								&& (elInfo.name == undefined || elInfo.name == siblings[i].name)) {
							idx++;
						}
					}

					return idx;
				}

				let _getElementPathFromContainer = function(containerEl, el) {
					/**
					 * 해당 Dom의 Selector 문자열을 구한다.
					 * 
					 * @param containerEl :
					 *            경로를 구한 기준이 되는 container element(없을경우 body
					 *            element까지의 경로)
					 * @param el :
					 *            경로를 구하고자 하는 element
					 */
					let absPath = "";
					let relativePath = "";
					let isSplit = false;

					let arrRout = [ $(el)[0] ];
					$(el).parents().each(function() {
						arrRout.push(this);
					});

					let infoArr = [];
					let isFin = false;
					let fnReversStr = (str) => { return str.split("").reverse().join(""); }
					for (let i = 0; i < arrRout.length; i++) {

						let nodeName = arrRout[i].nodeName.toLowerCase();
						let siblings = $(arrRout[i]).siblings(nodeName);
						let siblingCnt = siblings.length;

						let tagname = "", attrId = "", attrName = "", idx = "";

						if ($(arrRout[i]).attr("id") != undefined) {
							attrId = `#${$(arrRout[i]).attr("id")}`;
						} else {
							// nodeName이 같은 항목의 index만 필요 함.
							// name이 있는 항목인 경우 name까지 같은 것들의 index만 필요 함.
							if (!isFin && $(arrRout[i]).attr("name") != undefined) {
								attrName = `[name=${$(arrRout[i]).attr("name")}]`;
							}
							if (siblingCnt > 0) {
								// 루프구간
								let nodeIdx = _getNodeIdx(arrRout[i]);
								if (nodeIdx >= 0) {
									// -------------------------------------------------------------------------------------------
									// jquery가 없는 사이트도 있다.
									// document.querySelector()로 사용 가능한 selector를 생성하도록 한다.
									// :nth-of-type의 index는 1부터 시작 됨.
									idx = `:nth-of-type(${nodeIdx+1})`;
									// -------------------------------------------------------------------------------------------
								}
								isSplit = true;
							}
						}
						
						if(i+1<arrRout.length && "body" == arrRout[i+1].nodeName.toLowerCase()) {
							isSplit = true;
						}
						
						isFin = (nodeName == "html") || (containerEl != undefined && $(containerEl)[0] == arrRout[i]);
						if (isFin) {
							infoArr.push({
								revAbsPath : absPath,
								relativePath : relativePath
							});
							relativePath = "";
							break;
						} else {
							tmpPath = `${nodeName}${attrId}${attrName}${idx}`;
							absPath = (absPath == "") ? tmpPath : `${tmpPath} > ${absPath}`;
							relativePath = (relativePath == "") ? tmpPath : `${tmpPath} > ${relativePath}`;
						}
						if (isSplit) {
							infoArr.push({
								revAbsPath : absPath,
								relativePath : relativePath
							});
							relativePath = "";
							isSplit = false;
						}
					}

					// 절대경로를 수정 한다.(하위에서 부터 경로 -> Body 부터 경로)
					let retArr = [];
					let revPath = fnReversStr(infoArr[infoArr.length - 1].revAbsPath);
					for (let i = 0; i < infoArr.length; i++) {
						let pathTmp = revPath.replace(fnReversStr(infoArr[i].revAbsPath), "");
						if (pathTmp.indexOf(" > ") == 0) {
							pathTmp = pathTmp.replace(" > ", "");
						}
						infoArr[i].absPath = fnReversStr(pathTmp);

						// 반환 데이터 생성 //
						retArr.push({
							absPath : infoArr[i].absPath,
							relativePath : infoArr[i].relativePath
						});
					}

					return retArr;

					// end _getElementPathFromContainer //
				};

				if (containerEl == undefined) {
					containerEl = $("html");
				}
				return _getElementPathFromContainer(containerEl, el);
			},
			name : "util"
		},
		name : "$contents"
	}; // eof tab

	$(document).ready(function() {
		$contents.init();
	});
})();
