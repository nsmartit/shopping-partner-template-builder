(function() {
	$actionPlayer = {
		init : function($jsRecordData) {
			$data.storage.io.core.group.set($jsRecordData);
		},
		play : function() {

			const $actions = $data.storage.io.actions;
			
			const actionList = $actions.get();
			let retDataArr = [];
			let loopStack = [];
			let i = 0;
			
			
			let fnAbsPathByAlias = function(alias){
				const $items = $data.storage.io.items;
				const $util = $data.util;

				const items = $items.get();
				let findKey = Array.from(Object.keys(items)).find(o=>{return items[o].alias == alias});
				let findItem =  items[findKey];
				return findItem != undefined ? $util.getFullPath(findItem) : undefined;
			};
			
			let fnAbsPathByUUID = function(uuid){
				const $items = $data.storage.io.items;
				const $util = $data.util;
				
				const items = $items.get();
				let findItem =  items[uuid];
				return findItem != undefined ? $util.getFullPath(findItem) : undefined;
			};
			
			let fnAction = function() {
				if(i>=actionList.length) {
					console.log("end");
					return;
				}
//				for(let i=0; i<actionList.length; i++) {
					let action = actionList[i].action;
					let alias = actionList[i].alias;
					let condType = actionList[i].condType;
					let condVal = actionList[i].condVal;
					let uuid = actionList[i].uuid;
//debugger;

					switch(action) {
					case "startLoop": {
							let loopCnt = 0;
							let loopTarget = [];

							switch(condType) {
							case "element":
									// 타겟 Element를 구함.
									// 해당 Element의 Node
									let jsCssPath = fnAbsPathByAlias(condVal);
									if(jsCssPath != undefined) {
										let jsObj = $(jsCssPath);
										loopTarget = jsObj.parent().find(jsObj[0].nodeName.toLowerCase());
										loopCnt = loopTarget.length;
									} else {
										throw Exception("해당 Alias의 항목을 찾을 수 없습니다.");
									}
								break;
							case "count":
									loopCnt = Number(condVal);
								break;
							}
								
							loopStack.push({
								baseIdx : i,				/*loop가 시작된 정보값의 배열 위치*/
								loopIdx : 0,				/*loop index*/
								loopType : condType,		/*반복 유형 : element(특정항목의 형제노드를 반복 횟수반복) / count(횟수만큼 반복) */
								loopAlas : condVal,			/*반복할 항목의 Alias :: 해당 Alias를 만날경우.. loopTarget으로 대체 함.*/
								loopTarget : loopTarget,	/*반복대상목록 : loopType이 element인 경우 alias 항목을 대체할 대상 목록 */
								loopCnt : loopCnt			/*반복 실행할 횟수 : loopType이 count인 경우 loopIdx가 변할 횟수.. */
							});
							
						}
						break;
					case "endLoop": {
							let loopInfo = loopStack[loopStack.length-1];
							if(++loopInfo.loopIdx > loopInfo.loopCnt) {
								loopStack.pop();
							}else{
								i=loopInfo.baseIdx;
								loopStack[loopStack.length-1] = loopInfo;
							}
						}
						break;
					case "click":
						let isLooping = loopStack.length>0;
						let targetEl = undefined;
						if(isLooping) {
							// 현재 항목의 element에서 loopStack의 element가 잇는지 조회 함.
							// 현재 항목 대신 loopElemt 항목으로 click 하게 해야 함.
							let loopInfo = loopStack[loopStack.length-1];
							switch(loopInfo.loopType) {
							case "element": {
								targetEl = loopInfo.loopTarget[loopInfo.loopIdx];
							}
							break;
							default:{
								targetEl = $(fnAbsPathByUUID(uuid));
							}
							break;

							}
						} else {
							
						}
//debugger;
						if(targetEl != undefined) {
							targetEl.click();
						}
						break;
					case "read":
						break;
					}
					i++;
					setTimeout(fnAction, 1);
				}
//				return retDataArr;
//			}
			

			return fnAction();
		},
		name : "$actionPlayer"
	}
})();
